package diskworld.objectTree;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import diskworld.diskcomplexes.Disk;
import diskworld.diskcomplexes.DiskType;
import diskworld.interfaces.FrictionModel;
import diskworld.linalg2D.Point;

public class ObjectTreeNode {

	private final ObjectTreeNode ancestor;
	private final ObjectTree tree;
	private final List<ObjectTreeNode> children;
	private final List<MergeSet> mergeSets;
	private final double positionAngle;
	private Disk disk;
	private double radius, rotationAngle;

	ObjectTreeNode(ObjectTreeNode ancestor, ObjectTree tree, double positionAngle, double radius) {
		this.ancestor = ancestor;
		this.tree = tree;
		children = new LinkedList<ObjectTreeNode>();
		this.positionAngle = positionAngle;
		this.radius = radius;
		this.rotationAngle = 0.0;
		if (ancestor != null)
			ancestor.addChild(this);
		this.disk = null;
		mergeSets = new LinkedList<MergeSet>();
	}

	public ObjectTreeNode createChild(double positionAngle, double radius, DiskType diskType) {
		ObjectTreeNode child = new ObjectTreeNode(this, tree, positionAngle, radius);
		Disk disk = tree.getDiskComplexes().addNewDisk(tree.getDiskComplex(), child.calcPosX(), child.calcPosY(), radius, child.calcAngle(), diskType);
		child.setDisk(disk);
		return child;
	}

	void setDisk(Disk disk) {
		this.disk = disk;
	}

	private void addChild(ObjectTreeNode child) {
		children.add(child);
	}

	public boolean isRoot() {
		return ancestor == null;
	}

	public void setRadiusAndRotationAngle(double newRadius, double newRotationAngle) {
		this.rotationAngle = newRotationAngle;
		this.radius = newRadius;
	}

	public void setActivity(double activity) {
		disk.setActivity(activity);
	}

	/**
	 * Get the position of this node relative to the root, assuming the root nodes angle has been set to a specified value
	 * 
	 * @param rootAngle
	 *            the angle that the root node is supposed to have
	 * @return a triple of doubles: relative position x, relative position y, angle
	 */
	public double[] calcAbsolutePosition(double rootPosition[]) {
		if (ancestor == null) {
			return rootPosition.clone();
		}
		double res[] = ancestor.calcAbsolutePosition(rootPosition);
		double angle = res[Pos.ANGLE] + positionAngle;
		double d = distanceToAncestor();
		res[Pos.X] += deltaX(d, angle);
		res[Pos.Y] += deltaY(d, angle);
		res[Pos.ANGLE] = angle + rotationAngle;
		return res;
	}

	/**
	 * Get the angle offset of the node relative to the root node's angle
	 * 
	 * @return angle of this node, assuming the root node has angle = 0
	 */
	public double getRelativeAngle() {
		if (ancestor == null) {
			return 0.0;
		}
		return ancestor.getRelativeAngle() + positionAngle + rotationAngle;
	}

	private double distanceToAncestor() {
		return ancestor.getRadius() + radius;
	}

	private static double deltaX(double distance, double angle) {
		return distance * Math.cos(angle);
	}

	private static double deltaY(double distance, double angle) {
		return distance * Math.sin(angle);
	}

	//	void addMovedDisks(ObjectTreeNode stopNode, Map<Disk, double[]> positionMap) {
	//		if (this == stopNode)
	//			return;
	//		positionMap.put(disk, new double[3]);
	//		for (ObjectTreeNode child : children) {
	//			child.addMovedDisks(stopNode, positionMap);
	//		}
	//	}
	//	
	/**
	 * Calculate the position/angle of all disks in the subtree starting with this node, including positions of other ObjectTrees which are merged with this somewhere in the subtree.
	 * 
	 * @param ancestor
	 *            array[3] containing x coordinate, y coordinate, angle of ancestor's disk
	 * @param positionMap
	 *            calculated positions are put into this map as double[3] (x,y,angle) or double[4] (x,y,angle,radius) if radius != disk.getRadius()
	 */
	public void putPositions(double ancestorpos[], Map<Disk, double[]> positionMap) {
		double x = ancestorpos[Pos.X];
		double y = ancestorpos[Pos.Y];
		double angle = ancestorpos[Pos.ANGLE];
		if (ancestor != null) {
			double d = distanceToAncestor();
			angle += positionAngle;
			x += deltaX(d, angle);
			y += deltaY(d, angle);
		}
		angle += rotationAngle;
		double position[];
		if (radius != disk.getRadius())
			position = Pos.create(x, y, angle, radius);
		else
			position = Pos.create(x, y, angle);
		positionMap.put(disk, position);
		for (ObjectTreeNode child : children) {
			child.putPositions(position, positionMap);
		}
		for (MergeSet ms : mergeSets) {
			ms.putTransformedPositions(this, position, positionMap);
		}
	}

	/**
	 * Calculate the sum of floor contacts moments of disks corresponding to the subtree starting with this node, given ancestors position. If non-empty merge sets are encountered floor contacts of
	 * the other ObjectTrees merged with this are also taken into account. Momenta are calculated with respect to a given reference point.
	 * 
	 * @param stopNode
	 *            when this node is reached, recursion is stopped, so the floor contact of the subtree starting with stopNode is not taken into account
	 * @param rotCenter
	 *            reference point
	 * @return sum of momenta: sum radius*floor contact force
	 */
	double floorContactMomentum(ObjectTreeNode stopNode, Point rotCenter, FrictionModel frictionModel) {
		if (this == stopNode)
			return 0.0;
		double r = rotCenter.distance(disk.getX(), disk.getY());
		double ret = r * frictionModel.getFloorContact(disk);
		for (ObjectTreeNode child : children) {
			ret += child.floorContactMomentum(stopNode, rotCenter, frictionModel);
		}
		for (MergeSet ms : mergeSets) {
			ret += ms.floorContactMomentum(this, rotCenter, frictionModel);
		}
		return ret;
	}

	/**
	 * Calculate the sum of floor contacts forces of disks corresponding to the subtree starting with this node, given ancestors position. If non-empty merge sets are encountered floor contacts of the
	 * other ObjectTrees merged with this are also taken into account.
	 * 
	 * @param stopNode
	 *            when this node is reached, recursion is stopped, so the floor contact of the subtree starting with stopNode is not taken into account
	 * @return sum of floor contact forces
	 */
	double floorContact(ObjectTreeNode stopNode, FrictionModel frictionModel) {
		if (this == stopNode)
			return 0.0;
		double ret = frictionModel.getFloorContact(disk);
		for (ObjectTreeNode child : children) {
			ret += child.floorContact(stopNode, frictionModel);
		}
		for (MergeSet ms : mergeSets) {
			ret += ms.floorContact(this, frictionModel);
		}
		return ret;
	}

	public double getRadius() {
		return radius;
	}

	public double getRotationAngle() {
		return rotationAngle;
	}

	double getCurrentAngle() {
		return disk.getAngle();
	}

	double getCurrentPosX() {
		return disk.getX();
	}

	double getCurrentPosY() {
		return disk.getY();
	}

	double[] getCurrentPosition() {
		return Pos.create(disk.getX(), disk.getY(), disk.getAngle());
	}

	private double calcPosX() {
		return ancestor.getCurrentPosX() + deltaX(distanceToAncestor(), ancestor.getCurrentAngle() + positionAngle);
	}

	private double calcPosY() {
		return ancestor.getCurrentPosY() + deltaY(distanceToAncestor(), ancestor.getCurrentAngle() + positionAngle);
	}

	private double calcAngle() {
		return ancestor.getCurrentAngle() + positionAngle + rotationAngle;
	}

	/**
	 * Get access to the disk
	 * 
	 * @return disk assigned to this tree node
	 */
	public Disk getDisk() {
		return disk;
	}

}
