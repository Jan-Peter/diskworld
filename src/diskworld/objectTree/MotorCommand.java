package diskworld.objectTree;

import diskworld.visualization.Log;


public class MotorCommand {
	private final ObjectTreeNode node;
	private final double angleChange;
	private final double radiusFactor;
	private FixedEnd fixedEnd;

	public static enum FixedEnd { ROOT, LEAVES, FRICTION_DEPENDENT };

	private MotorCommand(ObjectTreeNode node, double angleChange, double radiusFactor, FixedEnd fixedEnd) {
		this.node = node;
		this.angleChange = angleChange;
		this.radiusFactor = radiusFactor;
		this.fixedEnd = fixedEnd;
		if (node.isRoot() && (angleChange != 0.0)) {
			Log.warning("cannot change root angle");
		}
		if (radiusFactor <= 0.0) {
			Log.warning("radius factor is not positive");
		}
	}

	//Creating a new AngleChangeMotorCommand with where the side that stays fixed is determined by friction
	public static MotorCommand createAngleChange(ObjectTreeNode node, double angleChange) {
		return new MotorCommand(node, angleChange, 1.0, FixedEnd.FRICTION_DEPENDENT);
	}

	//Creating a new AngleChangeMotorCommand specifying which part shall stay fixed 
	public static MotorCommand createAngleChange(ObjectTreeNode node, double angleChange, boolean rootStaysFixed) {
		return new MotorCommand(node, angleChange, 1.0, rootStaysFixed ? FixedEnd.ROOT : FixedEnd.LEAVES);
	}

	public static MotorCommand createRadiusChange(ObjectTreeNode node, double radiusFactor) {
		return new MotorCommand(node, 0.0, radiusFactor, FixedEnd.FRICTION_DEPENDENT);
	}

	public static MotorCommand createRadiusChange(ObjectTreeNode node, double radiusFactor, boolean rootStaysFixed) {
		return new MotorCommand(node, 0.0, radiusFactor, rootStaysFixed ? FixedEnd.ROOT : FixedEnd.LEAVES);
	}

	public ObjectTreeNode getNode() {
		return node;
	}

	public double getAngleChange() {
		return angleChange;
	}

	public double getRadiusFactor() {
		return radiusFactor;
	}

	public boolean isRotation() {
		return angleChange != 0.0;
	}

	public void doAction(double rootPos[]) {
		executeChangesWithRootAdjustment(rootPos, angleChange, radiusFactor);
	}

	public void undoAction(double rootPos[]) {
		executeChangesWithRootAdjustment(rootPos, -angleChange, 1.0/radiusFactor);
	}

	private void executeChangesWithRootAdjustment(double rootPos[], double angleChange, double radiusFactor) {
		switch (fixedEnd) {
		case FRICTION_DEPENDENT:
			throw new Error("Trying to execute a MotorCommand before determining the moving parts");

		case ROOT:
			executeChanges(node, angleChange, radiusFactor);		
			break;

		case LEAVES:			
			double beforePos[] = node.calcAbsolutePosition(rootPos);
			executeChanges(node, angleChange, radiusFactor);
			rootPos[Pos.ANGLE] -= angleChange;
			double afterPos[] = node.calcAbsolutePosition(rootPos);
			rootPos[Pos.X] += beforePos[Pos.X] - afterPos[Pos.X]; 
			rootPos[Pos.Y] += beforePos[Pos.Y] - afterPos[Pos.Y];
			break;
		}
	}

	private static void executeChanges(ObjectTreeNode node, double angleChange, double radiusFactor) {
		node.setRadiusAndRotationAngle(node.getRadius()*radiusFactor,node.getRotationAngle()+angleChange);
	}

	public boolean determineFixedEndByFriction(){
		return fixedEnd == FixedEnd.FRICTION_DEPENDENT;
	}

	public void setFixedEnd(FixedEnd fixedEnd) {
		this.fixedEnd = fixedEnd;
	}


}
