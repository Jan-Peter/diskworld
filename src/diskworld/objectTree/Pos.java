package diskworld.objectTree;

public class Pos {
	public static final int X = 0;
	public static final int Y = 1;
	public static final int ANGLE = 2;
	public static final int RADIUS = 3; // the radius entry is optional 
	
	public static double[] create(double x, double y, double angle) {
		return new double[] {x,y,angle}; 
	}
	
	public static double[] create(double x, double y, double angle, double radius) {
		return new double[] {x,y,angle,radius}; 
	}
	
	public static boolean hasRadius(double[] pos) {
		return pos.length > RADIUS;
	}

}
