package diskworld.objectTree;

import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import diskworld.diskcomplexes.DiskType;
import diskworld.environment.Environment;

public class TreeConstructionDescription {

	private final DiskType[] diskTypes;
	private final double rootRadius;
	private final int rootTypeIndex;
	private final List<DescriptionItem> items;

	public static class DescriptionItem {
		private int parentIndex;
		private double positionAngle;
		private double radius;
		private int diskTypeIndex;

		DescriptionItem(int parentIndex,
				double positionAngle,
				double radius,
				int diskTypeIndex) {
			this.positionAngle = positionAngle;
			this.parentIndex = parentIndex;
			this.radius = radius;
			this.diskTypeIndex = diskTypeIndex;
		}

		ObjectTreeNode createNode(Vector<ObjectTreeNode> existingNodes, DiskType[] diskTypes, double radiusFactor) {
			ObjectTreeNode parent;
			if (parentIndex < 0) {
				parent = existingNodes.get(existingNodes.size() + parentIndex);
			} else {
				parent = existingNodes.get(parentIndex);
			}
			if (parent == null)
				throw new IllegalArgumentException("Index not in existing nodes vector range");
			DiskType dt = getDiskType(diskTypeIndex, diskTypes);
			return parent.createChild(positionAngle, radius * radiusFactor, dt);
		}
	}

	private static DiskType getDiskType(int diskTypeIndex, DiskType[] diskTypes) {
		if ((diskTypeIndex < 0) || (diskTypeIndex >= diskTypes.length)) {
			throw new IllegalArgumentException("Index not in disk type range");
		}
		return diskTypes[diskTypeIndex];
	}

	public TreeConstructionDescription(DiskType[] diskTypes, double rootRadius, int rootTypeIndex) {
		this.diskTypes = diskTypes;
		this.rootRadius = rootRadius;
		this.rootTypeIndex = rootTypeIndex;
		this.items = new LinkedList<TreeConstructionDescription.DescriptionItem>();
	}

	public TreeConstructionDescription(DiskType[] diskTypes, String descriptionString) {
		String split[] = descriptionString.split(";");
		if (split.length == 0) {
			throw new IllegalArgumentException("expected at least one item in " + descriptionString);
		}
		this.diskTypes = diskTypes;
		String splitRoot[] = split[0].split(":");
		if (splitRoot.length != 2) {
			throw new IllegalArgumentException("expected rootRadius:rootTypeIndex instead of " + split[0]);
		}
		try {
			rootRadius = Double.parseDouble(splitRoot[0].trim());
			rootTypeIndex = Integer.parseInt(splitRoot[1].trim());
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("number format incorrect in " + split[0]);
		}
		this.items = new LinkedList<TreeConstructionDescription.DescriptionItem>();
		for (int i = 1; i < split.length; i++) {
			parseItem(split[i]);
		}
	}

	private void parseItem(String string) {
		String split[] = string.split(":");
		if (split.length != 4) {
			throw new IllegalArgumentException("expected parentIndex:positionAngle:radius:diskTypeIndex instead of " + string);
		}
		int parentIndex;
		double positionAngle;
		double radius;
		int diskTypeIndex;
		try {
			parentIndex = Integer.parseInt(split[0].trim());
			positionAngle = Double.parseDouble(split[1].trim());
			radius = Double.parseDouble(split[2].trim());
			diskTypeIndex = Integer.parseInt(split[3].trim());
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("number format incorrect in " + string);
		}
		addItem(parentIndex, positionAngle, radius, diskTypeIndex);
	}

	public void addItem(int parentIndex, double positionAngle, double radius, int diskTypeIndex) {
		items.add(new DescriptionItem(parentIndex, positionAngle, radius, diskTypeIndex));
	}

	public void addItem(int parentIndex, double positionAngle, double radius, DiskType diskType) {
		int diskTypeIndex = -1;
		for (int i = 0; i < diskTypes.length; i++) {
			if (diskType.equals(diskTypes[i])) {
				diskTypeIndex = i;
			}
		}
		if (diskTypeIndex < 0)
			throw new IllegalArgumentException("DiskType not in array: " + diskType);
		items.add(new DescriptionItem(parentIndex, positionAngle, radius, diskTypeIndex));
	}

	public ObjectTree create(Environment environment, double rootx, double rooty, double orientation, double scaleFactor) {
		DiskType rootType = getDiskType(rootTypeIndex, diskTypes);
		ObjectTree res = ObjectTree.createNewObjectTree(environment, rootRadius * scaleFactor, rootx, rooty, rootType);
		Vector<ObjectTreeNode> nodes = new Vector<ObjectTreeNode>();
		nodes.add(res.getRoot());
		for (DescriptionItem di : items) {
			nodes.add(di.createNode(nodes, diskTypes, scaleFactor));
		}
		res.getDiskComplex().getCoordinates().setAngle(orientation);
		return res;
	}

}
