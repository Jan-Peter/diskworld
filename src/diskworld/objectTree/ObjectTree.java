package diskworld.objectTree;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;

import diskworld.diskcomplexes.Collision;
import diskworld.diskcomplexes.Disk;
import diskworld.diskcomplexes.DiskComplex;
import diskworld.diskcomplexes.DiskComplexEnsemble;
import diskworld.diskcomplexes.DiskModification;
import diskworld.diskcomplexes.DiskType;
import diskworld.environment.Environment;
import diskworld.environment.PhysicsParameters;
import diskworld.interfaces.FrictionModel;
import diskworld.linalg2D.Point;
import diskworld.objectTree.MotorCommand.FixedEnd;

public class ObjectTree {

	private final ObjectTreeNode root;
	private final DiskComplexEnsemble diskComplexes;
	private final DiskComplex diskComplex;

	private ObjectTree(double rootRadius, DiskComplex diskComplex,
			DiskComplexEnsemble diskComplexEnsemble) {
		this.diskComplexes = diskComplexEnsemble;
		this.diskComplex = diskComplex;
		this.root = new ObjectTreeNode(null, this, 0.0, rootRadius);
	}

	public static ObjectTree createNewObjectTree(Environment env,
			double rootRadius, double rootx, double rooty, DiskType rootType) {
		DiskComplex dc = env.getDiskComplexesEnsemble().createNewDiskComplex();
		ObjectTree tree = new ObjectTree(rootRadius, dc,
				env.getDiskComplexesEnsemble());
		Disk disk = env.getDiskComplexesEnsemble().addNewDisk(dc, rootx, rooty,
				rootRadius, 0 /* angle */, rootType);
		tree.getRoot().setDisk(disk);
		return tree;
	}

	public ObjectTreeNode getRoot() {
		return root;
	}

	public void executeMotorCommands(List<MotorCommand> commands,
			double timeStep, PhysicsParameters physicsParameters, FixedEnd fixedEnd) {
		for (MotorCommand mc : commands)
			mc.setFixedEnd(fixedEnd);
		executeMotorCommands(commands, timeStep, physicsParameters);
		
	}
	public void executeMotorCommands(List<MotorCommand> commands,
			double timeStep, PhysicsParameters physicsParameters) {
		// determine for each command if the root node stays fixed or the
		// modified node stays fixed
		for (MotorCommand command : commands) {
			determineMovingPart(command, physicsParameters.getFrictionModel());
		}
		// delete motor commands that would lead to self-collisions, iterate
		// this because removing some commands may introduce new collisions
		boolean hasSelfCollisions;

		do {
			List<Collision> selfCollisions = getSelfCollisions(predictNewPositions(
					commands, true));

			hasSelfCollisions = !selfCollisions.isEmpty();

			if (hasSelfCollisions) {
				commands = avoidCollisions(commands, selfCollisions);
			}

		} while (hasSelfCollisions);
		// get all collisions with other DiskComplexes (in the old state, i.e.
		// before the commands are executed)
		List<Collision> existingCollisions = diskComplexes
				.getCollisionDetector().getNonSelfCollisions(diskComplex);
		// collisions that would be compressed generate an impulse flow
		evaluateCompressionImpulseFlow(existingCollisions,
				predictNewPositions(commands, true), timeStep);
		// remove motor commands that lead to compression of existing collisions
		commands = avoidCollisions(commands, existingCollisions);
		// now the list of commands is final and not modified any longer
		Map<Disk, double[]> newPositions = predictNewPositions(commands, false);
		// apply friction impulses due to disk displacements
		applyFrictionImpulses(newPositions, timeStep,
				physicsParameters.getFrictionModel());
		// let impulses in existing collisions propagate
		exchangeCollisionImpulses(existingCollisions, physicsParameters);
		// realize the commands' effects
		setNewPositions(newPositions);
		// let impulses propagate in the new set of collisions
		List<Collision> afterCollisions = diskComplexes.getCollisionDetector()
				.getNonSelfCollisions(diskComplex);
		exchangeCollisionImpulses(afterCollisions, physicsParameters);
	}

	private void determineMovingPart(MotorCommand command,
			FrictionModel frictionModel) {
		if (command.determineFixedEndByFriction()) {
			ObjectTreeNode node = command.getNode();
			double contact1, contact2;
			if (command.isRotation()) {
				Point rotCenter = new Point(node.getCurrentPosX(),
						node.getCurrentPosY());
				contact1 = root.floorContactMomentum(node, rotCenter,
						frictionModel);
				contact2 = node.floorContactMomentum(null, rotCenter,
						frictionModel);
			} else {
				contact1 = root.floorContact(node, frictionModel);
				contact2 = node.floorContact(null, frictionModel);
			}
			command.setFixedEnd(contact2 > contact1 ? FixedEnd.LEAVES
					: FixedEnd.ROOT);
		}
	}

	private Map<Disk, double[]> predictNewPositions(
			List<MotorCommand> commands, boolean undoActions) {
		Map<Disk, double[]> res = new HashMap<Disk, double[]>();
		if (commands.isEmpty())
			// without commands there are no new positions
			return res;

		double rootPos[] = root.getCurrentPosition();
		for (MotorCommand command : commands) {
			command.doAction(rootPos);
		}

		root.putPositions(rootPos, res);
		if (undoActions) {
			ListIterator<MotorCommand> reversIter = commands
					.listIterator(commands.size());
			while (reversIter.hasPrevious()) {
				reversIter.previous().undoAction(rootPos);
			}
		}
		return res;
	}

	private List<Collision> getSelfCollisions(Map<Disk, double[]> positions) {
		List<Collision> res = new LinkedList<Collision>();
		for (Entry<Disk, double[]> e1 : positions.entrySet()) {
			Disk d1 = e1.getKey();
			double p1[] = e1.getValue();
			boolean cont = true;
			for (Iterator<Entry<Disk, double[]>> i = positions.entrySet()
					.iterator(); cont && i.hasNext();) {
				Entry<Disk, double[]> e2 = i.next();
				Disk d2 = e2.getKey();
				double p2[] = e2.getValue();
				if (d1 == d2) {
					cont = false;
				} else {
					Collision c = getCollision(d1, p1, d2, p2);
					if (c != null) {
						res.add(c);
					}
				}
			}
		}
		return res;
	}

	private Collision getCollision(Disk d1, double[] p1, Disk d2, double[] p2) {
		double r1 = Pos.hasRadius(p1) ? p1[Pos.RADIUS] : d1.getRadius();
		double r2 = Pos.hasRadius(p2) ? p2[Pos.RADIUS] : d2.getRadius();
		return Collision.roundObjectCollision(d1, p1[Pos.X], p1[Pos.Y], r1, d2,
				p2[Pos.X], p2[Pos.Y], r2);
	}

	private List<MotorCommand> avoidCollisions(List<MotorCommand> commands,
			List<Collision> collisions) {
		LinkedList<MotorCommand> res = new LinkedList<MotorCommand>();
		for (MotorCommand command : commands) {
			res.add(command);
			if (involvedInCollisions(res, collisions)) {
				res.removeLast();
			}
		}
		return res;
	}

	private boolean involvedInCollisions(List<MotorCommand> commands,
			List<Collision> collisions) {
		// check if the current list of commands produces self collisions
		Map<Disk, double[]> newPositions = predictNewPositions(commands, true);
		if (!getSelfCollisions(newPositions).isEmpty())
			return true;
		// check if the current list of commands takes colliding disks closer to
		// each other
		for (Collision collision : collisions) {
			if (distanceChange(collision, newPositions) < -Collision.EPSILON_SPEED_DIFF) {
				return true;
			}
		}
		return false;
	}

	private void evaluateCompressionImpulseFlow(List<Collision> collisions,
			Map<Disk, double[]> newPositions, double timeStep) {
		for (Collision c : collisions) {
			double distChange = distanceChange(c, newPositions);
			if (distChange < 0.0) {
				c.setEgoMotion(-distChange / timeStep);
			}
		}
	}

	private double distanceChange(Collision c, Map<Disk, double[]> newPositions) {
		Disk d1 = c.getObj1();
		double distChange1 = projectedDelta(d1, newPositions.get(d1), c);
		double distChange2 = 0.0;
		if (c.getObj2() instanceof Disk) {
			Disk d2 = (Disk) c.getObj2();
			distChange2 = projectedDelta(d2, newPositions.get(d2), c);
		}
		return distChange2 - distChange1;
	}

	private double projectedDelta(Disk d, double[] newpos, Collision c) {
		if (newpos == null)
			return 0.0;
		double dx = newpos[0] - d.getX();
		double dy = newpos[1] - d.getY();
		return c.getProjection(dx, dy);
	}

	private void setNewPositions(Map<Disk, double[]> newPositions) {
		for (Entry<Disk, double[]> e : newPositions.entrySet()) {
			Disk disk = e.getKey();
			double pos[] = e.getValue();
			DiskModification diskMod;
			if (Pos.hasRadius(pos))
				diskMod = new DiskModification(disk, pos[Pos.X], pos[Pos.Y],
						pos[Pos.ANGLE], pos[Pos.RADIUS]);
			else
				diskMod = new DiskModification(disk, pos[Pos.X], pos[Pos.Y],
						pos[Pos.ANGLE]);
			diskComplexes.performDiskModification(diskMod);
		}
	}

	private void exchangeCollisionImpulses(List<Collision> collisions,
			PhysicsParameters physicsParameters) {
		diskComplexes.exchangeImpulsesInCollisions(collisions,
				physicsParameters);
	}

	private void applyFrictionImpulses(Map<Disk, double[]> newPositions,
			double timeStep, FrictionModel frictionModel) {
		diskComplex
				.applyFrictionImpulses(newPositions, timeStep, frictionModel);
	}

	DiskComplexEnsemble getDiskComplexes() {
		return diskComplexes;
	}

	public DiskComplex getDiskComplex() {
		return diskComplex;
	}

}
