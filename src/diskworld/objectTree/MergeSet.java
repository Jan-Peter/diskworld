package diskworld.objectTree;

import java.util.Map;
import java.util.Map.Entry;

import diskworld.diskcomplexes.Disk;
import diskworld.diskcomplexes.DiskComplex;
import diskworld.interfaces.FrictionModel;
import diskworld.linalg2D.Point;

public class MergeSet {

	private Map<Object,DiskComplex> mergedDiskComplexes; // key: either ObjectTreeNode or DiskComplex 
	
	public double floorContactMomentum(ObjectTreeNode node, Point rotCenter, FrictionModel frictionModel) {
		double ret = 0;
		for (Entry<Object, DiskComplex> e : mergedDiskComplexes.entrySet()) {
			if (e.getKey() != node) { 
				for (Disk d : e.getValue().getDisks()) {
					double r = rotCenter.distance(d.getX(), d.getY());
					ret += r*frictionModel.getFloorContact(d);
					
				}
			}
		}
		return ret;
	}

	public double floorContact(ObjectTreeNode node, FrictionModel frictionModel) {
		double ret = 0;
		for (Entry<Object, DiskComplex> e : mergedDiskComplexes.entrySet()) {
			if (e.getKey() != node) { 
				for (Disk d : e.getValue().getDisks()) {
					ret += frictionModel.getFloorContact(d);
				}
			}
		}
		return ret;
	}

	/**
	 * Determine the transformation that maps given node to the specified coordinates/angle. Then apply this transformation to all
	 * merged DiskComplexes, except the one to which the given node belongs.
	 *  
	 * @param node the node that serves as reference, and whose corresponding DiskComplex is not to be touched
	 * @param nodeNewPosition new position of specified node
	 * @param positionMap the map into which all positions shall be put
	 */
	public void putTransformedPositions(ObjectTreeNode node, double nodeNewPosition[], Map<Disk, double[]> positionMap) {
		// establish transform nx = cos(da)*x - sin(da)*y + dx, ny = sin(da)*x + cos(da)*y +dy, na = a + da:
		double deltaAngle = nodeNewPosition[Pos.ANGLE]-node.getCurrentAngle();
		double s = Math.sin(deltaAngle);
		double c = Math.cos(deltaAngle);
		double x = node.getCurrentPosX();
		double y = node.getCurrentPosY();
		double dx = nodeNewPosition[Pos.X] - c*x + s*y;
		double dy = nodeNewPosition[Pos.Y] - s*x - c*y;		
		for (Entry<Object, DiskComplex> e : mergedDiskComplexes.entrySet()) {
			if (e.getKey() != node) { 
				for (Disk d : e.getValue().getDisks()) {
					x = d.getX();
					y = d.getY();
					double nx = c*x-s*y+dx;
					double ny = s*x+c*y+dy;
					double na = d.getAngle()+deltaAngle;
					positionMap.put(d,Pos.create(nx, ny, na));
				}
			}
		}
	}

}
