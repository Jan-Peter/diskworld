package diskworld.interfaces;

import diskworld.linalg2D.Point;

public interface CollisionEventHandler {
	public void collision(CollidableObject collidableObject, Point collisionPoint);
}
