package diskworld.interfaces;

import diskworld.diskcomplexes.Disk;


// Interface 
public interface FrictionModel {

	public boolean frictionIsGlobal(); // determines if the FrictionModel is Global or Local

	public double getGlobalFrictionCoefficient();	// returns the frictionConstant, should only be called for GlobalFrictionModels

	double getFloorContact(Disk d);  // returns Mass of disk d * frictionConstant

	double[] getFrictionForce(Disk d, double speedx, double speedy); // returns a double array, containing the speed in x- and y-direction
																	 // dependent on speedx and speedy
}
