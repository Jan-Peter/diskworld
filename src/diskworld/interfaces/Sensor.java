package diskworld.interfaces;

import diskworld.diskcomplexes.Disk;
import diskworld.visualization.VisualisationItem;

/**
 * Interface for sensors. A sensor should be generic, which means not hold any reference to the Disk to which it is attached to. In particular, it should be possible to use the same Sensor object
 * shared for multiple Disks. In order to compute the actual sensor measurement, the Disk and the Floor are passed as arguments.
 */
public interface Sensor {

	/**
	 * Minimal sensor value
	 * 
	 * @return the minimal possible sensor measurement (Double.negativeInfinity if no minimum exists)
	 */
	public double getMinValue();

	/**
	 * Maximal sensor value
	 * 
	 * @return the maximal possible sensor measurement (Double.positiveInfinity if no minimum exists)
	 */
	public double getMaxValue();

	/**
	 * Dimensionality of the sensor
	 * 
	 * @return the number of values produced by this sensor in every time step
	 */
	public int getDimension();

	/**
	 * Get the current sensor value (assuming the sensor is attached to the specified disk)
	 * 
	 * @param disk
	 *            the Disk to which the sensor is supposed to be attached to
	 * @param values
	 *            array of doubles (length==getDim()) that is supposed to be filled with the measurement results in the interval [getMinValue(),getMaxValue()]
	 */
	public void doMeasurement(Disk disk, double values[]);

	/**
	 * Creates a visualization item, to be attached to DiskTypes that use the sensor
	 * 
	 * @return
	 *         a new visualization item, or null if no visualization supported
	 */
	public VisualisationItem getVisualisationItem();

}
