package diskworld.interfaces;

import diskworld.diskcomplexes.Disk;
import diskworld.environment.Floor;
import diskworld.visualization.VisualisationItem;

/**
 * Interface for actuators. An actuator should be generic, which means it should not hold any reference to the Disk to which it is attached to. 
 * In particular, it should be possible to use the same Actuator object shared for multiple Disks. 
 * The control value send to the actuator is called "activity". The activity is a property of the disk object 
 * and is passed to the evaluateEffect() method to perform the actuators effect on the disk.   
 */
public interface Actuator {

	/**
	 * Minimal actuator value
	 *  
	 * @return the minimal possible actuator setting (Double.negativeInfinity if no minimum exists)
	 */
	public double getRangeMin();

	/**
	 * Maximal actuator value
	 *  
	 * @return the maximal possible actuator setting (Double.positiveInfinity if no minimum exists)
	 */
	public double getRangeMax();

	/**
	 * Initial/default actuator value
	 *  
	 * @return initial ("do nothing") value for the actuator
	 */
	public double getInitialActivity();

	/**
	 * Perform the effect of the Actuator on the given disk (possibly depending on the floor state)  
	 *  
	 * @param disk the Disk to which the sensor is supposed to be attached to
	 * @param floor current state of the floor grid
	 * @param activity the current activation value, a double in [getRangeMin(),getRangeMax()]
	 */
	public void evaluateEffect(Disk disk, Floor floor, double activity);
	
	/**
	 * Creates a visualization item, to be attached to DiskTypes that use the sensor
	 * 
	 * @return
	 *         a new visualization item, or null if no visualization supported
	 */
	public VisualisationItem getVisualisationItem();

}

