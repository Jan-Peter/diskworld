package diskworld.interfaces;

import java.util.LinkedList;

import diskworld.diskcomplexes.Collision;
import diskworld.diskcomplexes.DiskComplex;

public interface CollisionDetector {

	public LinkedList<Collision> getNonSelfCollisions(DiskComplex dc);

	public LinkedList<Collision> getNonSelfCollisions();

	public boolean hasSelfCollisions(DiskComplex diskComplex);

}
