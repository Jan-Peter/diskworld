package diskworld.interfaces;

import diskworld.diskcomplexes.Disk;

/**
 * Interface for a Lookuptable that shall be called when the configuration of disks has 
 * changed. Methods are called for each changed disk individually.
 */
public interface DiskChangeListener {
	/**
	 * Called when a disk has changed its position
	 * 
	 * @param d the disk that has changed its place
	 */
	public void diskHasMoved(Disk d);

	/**
	 * Called when a disk was created
	 * 
	 * @param d the disk that has appeared
	 */
	public void diskWasAdded(Disk d);

	/**
	 * Called when a disk was removed
	 * 
	 * @param d the disk that has disappeared
	 */
	public void diskWasRemoved(Disk d);

	/**
	 * Called when a disk has changed its size
	 * 
	 * @param d the disk that has changed its radius
	 */
	public void diskHasChangedRadius(Disk d);	
	
}
