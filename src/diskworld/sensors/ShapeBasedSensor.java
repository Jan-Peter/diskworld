package diskworld.sensors;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.LinkedList;
import java.util.Set;

import diskworld.diskcomplexes.Disk;
import diskworld.diskcomplexes.DiskType;
import diskworld.environment.Environment;
import diskworld.environment.Floor;
import diskworld.interfaces.Sensor;
import diskworld.shape.Shape;
import diskworld.visualization.VisualisationItem;
import diskworld.visualization.VisualizationOption;
import diskworld.visualization.VisualizationOptions;
import diskworld.visualization.VisualizationSettings;

/**
 * @author Jan
 * 
 *         Base class for sensors that use shapes to select disks or floor cells
 */
public abstract class ShapeBasedSensor implements Sensor {

	private Environment environment;
	private ShapeTemplate sensorShapeTemplate;
	private Shape sensorShape;
	private Disk disk;
	private Floor floor;
	private String sensorName;
	private ShapeVisualisationVariant variants[];

	/**
	 * Generates a sensor based an a shape template and multiple visualisation variants.
	 * 
	 * @param environment
	 *            the environment in which the sensor is living
	 * @param sensorShape
	 *            a template for the sensor shape
	 * @param sensorName
	 *            a name for the sensor (used in visualisation option pop up menu)
	 * @param variants
	 *            array with at least two variants for visualisation
	 */
	public ShapeBasedSensor(Environment environment, ShapeTemplate sensorShape, String sensorName, ShapeVisualisationVariant variants[]) {
		this.environment = environment;
		this.sensorShapeTemplate = sensorShape;
		this.sensorName = sensorName;
		this.variants = variants;
	}

	/**
	 * Generates a sensor based an a shape template and a single set of visualisation parameters.
	 * 
	 * @param environment
	 *            the environment in which the sensor is living
	 * @param sensorShape
	 *            a template for the sensor shape
	 * @param sensorName
	 *            a name for the sensor (used in visualisation option pop up menu)
	 * @param visualisation
	 *            settings for the visualisation
	 */
	public ShapeBasedSensor(Environment environment, ShapeTemplate sensorShape, String sensorName, ShapeVisualisationVariant visualisation) {
		this(environment, sensorShape, sensorName, new ShapeVisualisationVariant[] { visualisation });
	}

	@Override
	public void doMeasurement(Disk disk, double[] values) {
		sensorShape = sensorShapeTemplate.getShape(disk.getX(), disk.getY(), disk.getRadius(), disk.getAngle());
		this.disk = disk;
		doMeasurement(values);
	}

	/**
	 * Return a visualisation item, to be attached to DiskTypes that use the sensor
	 * 
	 * @param disk
	 *            the disk that the cloned shape is supposed to be attached to
	 * @return
	 *         a new visualisation item
	 */
	public VisualisationItem getVisualisationItem() {
		return new VisualisationItem() {
			@Override
			public void draw(Graphics2D g, double centerx, double centery, double radius, double angle, double activity, double measurement[], VisualizationSettings settings, DiskType diskType) {
				VisualizationOption option = settings.getOptions().getOption(VisualizationOptions.GROUP_SENSORS, sensorName);
				// add sensor to visualisation options, if not yet there
				if (option == null) {
					settings.getOptions().addOption(VisualizationOptions.GROUP_SENSORS, sensorName, getVariantNames());
					option = settings.getOptions().getOption(VisualizationOptions.GROUP_SENSORS, sensorName);
				}
				sensorShape = sensorShapeTemplate.getShape(centerx, centery, radius, angle);
				if (option.isEnabled()) {
					ShapeVisualisationVariant visualisation;
					if (option.hasVariants()) {
						visualisation = variants[option.chosenVariantIndex()];
					} else {
						visualisation = variants[0];
					}
					Color fillcol = visualisation.getFillColor(measurement, settings);
					if (fillcol != null) {
						sensorShape.fill(g, fillcol, settings);
					}
					Color bordercol = visualisation.getBorderColor(measurement, settings);
					if (bordercol != null) {
						sensorShape.drawBorder(g, bordercol, settings);
					}
					visualisation.additionalVisualisation(g, measurement, sensorShape, centerx, centery, radius, angle, settings);
				}
			}
		};
	}

	// protected methods

	/**
	 * Abstract method, implemented by sub-classed to actually perform the measurement
	 * 
	 * @param values
	 *            array of doubles to be filled with the sensor measurement
	 */
	protected abstract void doMeasurement(double[] values);

	// helper methods that may be used by sub-classes to in getMeasurement()s 

	/**
	 * Provide shape
	 * 
	 * @return shape object
	 */
	protected Shape getShape() {
		return sensorShape;
	}

	/**
	 * Provide disk
	 * 
	 * @return disk object
	 */
	protected Disk getDisk() {
		return disk;
	}

	/**
	 * Provide indices of all floor tiles in the sensor shape
	 * 
	 * @return list of pairs (colIndex,rowIndex)
	 */
	protected LinkedList<int[]> getFloorIndicesInShape() {
		return environment.getFloorGrid().getCellIndicesIntersectingWithShape(sensorShape);
	}

	/**
	 * Provides a histogram of the floor tile types covered by the sensor shape
	 * 
	 * @return array of length 256, holding the absolute count of the floor types
	 */
	protected int[] getFloorTypeHistogram() {
		int[] histo = new int[256];
		for (int[] idx : getFloorIndicesInShape()) {
			histo[floor.getType(idx[0], idx[1])]++;
		}
		return histo;
	}

	/**
	 * Provide all disks that intersect with the sensor shape
	 * 
	 * @return a set of Disk objects
	 */
	protected Set<Disk> getDisksInShape() {
		return environment.getDiskGrid().getDisksIntersectingWithShape(sensorShape);
	}

	/**
	 * Create a string of the measurement array
	 * 
	 * @param measurement
	 *            the array of sensor values
	 * @param numDigits
	 *            number of digits to be shown
	 * @return formatted string
	 */
	protected static String getMeasurementString(double[] measurement, int numDigits) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < measurement.length; i++) {
			if (i > 0)
				sb.append(", ");
			sb.append(String.format("%." + numDigits + "f", measurement[i]));
		}
		return sb.toString();
	}

	/**
	 * Draw a string centered at given position
	 * 
	 * @param g
	 *            graphics object
	 * @param string
	 *            text
	 * @param x
	 *            x coordinate
	 * @param y
	 *            y coordinate
	 */
	protected static void drawStringCentered(Graphics2D g, String string, int x, int y) {
		Rectangle2D r = g.getFontMetrics().getStringBounds(string, null);
		int ascent = g.getFontMetrics().getAscent();
		g.drawString(string, x - (int) r.getWidth() / 2, y + ascent / 2);
	}

	/**
	 * Print sensor measurements on screen
	 * 
	 * @param g
	 *            graphics object
	 * @param x
	 *            x coordinate (center of text)
	 * @param y
	 *            y coordinate (center of text)
	 * @param measurement
	 *            the array of sensor values
	 * @param numDigits
	 *            number of digits to be shown
	 */
	protected static void drawMeasurement(Graphics2D g, int x, int y, double[] measurement, int numDigits) {
		drawStringCentered(g, getMeasurementString(measurement, numDigits), x, y);
	}

	// private methods 

	private String[] getVariantNames() {
		if (variants.length <= 1)
			return null;
		String res[] = new String[variants.length];
		for (int i = 0; i < res.length; i++) {
			res[i] = variants[i].getVariantName();
		}
		return res;
	}

}
