package diskworld.sensors;

import java.awt.Color;
import java.awt.Graphics2D;

import diskworld.shape.Shape;
import diskworld.visualization.VisualizationSettings;

public abstract class ShapeVisualisationVariant {
	private String variantName;

	/**
	 * Creates settings for visualising shapes.
	 * 
	 * @param variantName
	 *            the name of the variant (not used if only one variant exists)
	 */
	public ShapeVisualisationVariant(String variantName) {
		this.variantName = variantName;
	}

	/**
	 * Provides the name
	 * 
	 * @return name of variant
	 */
	public String getVariantName() {
		return variantName;
	}

	/**
	 * Provide the color for border.
	 * 
	 * @param measurement
	 *            the measurement or null (if {@link #requiresMeasurement()} returns false)
	 * @param settings
	 *            visualization settings
	 * @return
	 *         color in which the border shall be painted, or null if no border shall be painted
	 */
	/**
	 * @param measurement
	 * @return
	 */
	public abstract Color getBorderColor(double[] measurement, VisualizationSettings settings);

	/**
	 * Provide the color used to fill the shape.
	 * 
	 * @param measurement
	 *            the measurement or null (if {@link #requiresMeasurement()} returns false)
	 * @param settings
	 *            visualization settings
	 * @return
	 *         color in which the shape shall be filled, or null if filling shall occur
	 */
	public abstract Color getFillColor(double[] measurement, VisualizationSettings settings);

	/**
	 * Perform additional visualizations
	 * 
	 * @param g
	 *            the graphics object used to draw
	 * @param measurement
	 *            the measurement or null (if {@link #requiresMeasurement()} returns false)
	 * @param sensorShape
	 *            the current sensor shape
	 * @param settings
	 *            visualization settings
	 */
	public abstract void additionalVisualisation(Graphics2D g, double[] measurement, Shape sensorShape, double cx, double cy, double r, double angle, VisualizationSettings settings);
}
