package diskworld.sensors;

import diskworld.diskcomplexes.Disk;
import diskworld.environment.Environment;

public class DiskDistanceSensor extends ConeSensor {

	private static final double MIN_RANGE_RELATIVE = 1.5; // 50% more than disk radius
	private static final String SENSOR_NAME = "DiskDistance";
	private static final ShapeVisualisationVariant[] OPTIONS = new ShapeVisualisationVariant[] {
			GRAPHICAL_ARC_TEXT,
			GRAPHICAL_ARC,
			TEXT_VISUALISATION,
			TEXT_VISUALISATION_IN_DISK,
			NO_VALUE_VISUALISATION
	};

	private double maxValue;

	public DiskDistanceSensor(Environment environment, double centerAngle, double openingAngle, double maxRangeAbsolute) {
		super(environment, centerAngle, openingAngle, MIN_RANGE_RELATIVE, maxRangeAbsolute, SENSOR_NAME, OPTIONS);
		this.maxValue = maxRangeAbsolute;
	}

	@Override
	public double getMinValue() {
		return 0.0;
	}

	@Override
	public double getMaxValue() {
		return maxValue;
	}

	@Override
	public int getDimension() {
		return 1;
	}

	@Override
	protected void doMeasurement(double measurement[]) {
		double centerx = getDisk().getX();
		double centery = getDisk().getY();
		double min = maxValue;
		for (Disk d : getDisksInShape()) {
			double dx = d.getX() - centerx;
			double dy = d.getY() - centery;
			double dist = Math.sqrt(dx * dx + dy * dy) - d.getRadius();
			if (dist < min)
				min = dist;
		}
		measurement[0] = min;
	}
}
