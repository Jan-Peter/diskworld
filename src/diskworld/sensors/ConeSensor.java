package diskworld.sensors;

import java.awt.Color;
import java.awt.Graphics2D;

import diskworld.environment.Environment;
import diskworld.shape.CircleShape;
import diskworld.shape.RingSegmentShape;
import diskworld.shape.RingShape;
import diskworld.shape.Shape;
import diskworld.visualization.VisualizationSettings;

public abstract class ConeSensor extends ShapeBasedSensor {

	/**
	 * Sensor with a cone-shaped receptive field. Since special cases are handled by different implementations,
	 * different shape objects will be used. There are also specialised constructors to make this explicit.
	 * 
	 * @param environment
	 *            environment the sensor lives in
	 * @param centerAngle
	 *            the angle of the centre of the cone
	 * @param openingAngle
	 *            width of the cone
	 * @param minRangeRelativeToRadius
	 *            minimum distance (inner radius), specified in multiples of the disk radius
	 * @param maxRangeAbsolute
	 *            maximum distance (outer radius), in absolute units
	 * @param sensorName
	 *            a name for the sensor (used in options pop-up menu)
	 * @param visualisationOptions
	 *            the visualisation settings object
	 */
	public ConeSensor(
			Environment environment,
			double centerAngle,
			double openingAngle,
			double minRangeRelativeToRadius,
			double maxRangeAbsolute,
			String sensorName,
			ShapeVisualisationVariant[] visualisationOptions) {
		super(environment, getTemplate(centerAngle, openingAngle, minRangeRelativeToRadius, maxRangeAbsolute), sensorName, visualisationOptions);
	}

	/**
	 * Special case of opening angle = 2*PI (shape is a full ring).
	 * This special case uses a different (more efficient) implementation, also the visualisation has to be handled slightly differently.
	 * So it is preferable to call this constructor, if it is a-priori known that opening angle = 2*PI!
	 * 
	 * @param environment
	 *            environment the sensor lives in
	 * @param minRangeRelativeToRadius
	 *            minimum distance (inner radius), specified in multiples of the disk radius
	 * @param maxRangeAbsolute
	 *            maximum distance (outer radius), in absolute units
	 * @param sensorName
	 *            a name for the sensor (used in options pop-up menu)
	 * @param visualisationOptions
	 *            the visualisation settings object
	 */
	public ConeSensor(
			Environment environment,
			double minRangeRelativeToRadius,
			double maxRangeAbsolute,
			String sensorName,
			ShapeVisualisationVariant[] visualisationOptions) {
		super(environment, getRingTemplate(minRangeRelativeToRadius, maxRangeAbsolute), sensorName, visualisationOptions);
	}

	/**
	 * Special case of opening angle = 2*PI and minRange = 0 (shape is a circle).
	 * This special case uses a different (more efficient) implementation, also the visualisation has to be handled slightly differently.
	 * So it is preferable to call this constructor, if it is a-priori known that opening angle = 2*PI and minRange = 0!
	 * 
	 * @param environment
	 *            environment the sensor lives in
	 * @param maxRangeAbsolute
	 *            maximum distance (outer radius), in absolute units
	 * @param sensorName
	 *            a name for the sensor (used in options pop-up menu)
	 * @param visualisationOptions
	 *            the visualisation settings object
	 */
	public ConeSensor(
			Environment environment,
			double minRangeRelativeToRadius,
			String sensorName,
			ShapeVisualisationVariant[] visualisationOptions) {
		super(environment, getCircleTemplate(minRangeRelativeToRadius), sensorName, visualisationOptions);
	}

	private static ShapeTemplate getConeTemplate(final double centerAngle, final double openingAngle, final double minRangeRelativeToRadius, final double maxRangeAbsolute) {
		return new ShapeTemplate() {
			@Override
			public Shape getShape(double centerx, double centery, double radius, double angle) {
				return new RingSegmentShape(centerx, centery, minRangeRelativeToRadius * radius, maxRangeAbsolute, centerAngle - openingAngle * 0.5 + angle, openingAngle);
			}
		};
	}

	private static ShapeTemplate getTemplate(double centerAngle, double openingAngle, double minRangeRelativeToRadius, double maxRangeAbsolute) {
		if (openingAngle < 2.0 * Math.PI) {
			return getConeTemplate(centerAngle, openingAngle, minRangeRelativeToRadius, maxRangeAbsolute);
		}
		if (minRangeRelativeToRadius > 0.0) {
			return getRingTemplate(minRangeRelativeToRadius, maxRangeAbsolute);
		}
		return getCircleTemplate(maxRangeAbsolute);
	}

	private static ShapeTemplate getRingTemplate(final double minRangeRelativeToRadius, final double maxRangeAbsolute) {
		return new ShapeTemplate() {
			@Override
			public Shape getShape(double centerx, double centery, double radius, double angle) {
				return new RingShape(centerx, centery, minRangeRelativeToRadius * radius, maxRangeAbsolute);
			}
		};
	}

	private static ShapeTemplate getCircleTemplate(final double maxRangeAbsolute) {
		return new ShapeTemplate() {
			@Override
			public Shape getShape(double centerx, double centery, double radius, double angle) {
				return new CircleShape(centerx, centery, maxRangeAbsolute);
			}
		};
	}

	// visualization options to be used by subclasses 

	private static void drawArc(Graphics2D g, double[] measurement,
			Shape shape, double cx, double cy, VisualizationSettings settings) {
		double angles[] = shape.referenceAngles();
		int a1 = (int) Math.round(angles[0] * 180.0 / Math.PI);
		int a2 = (int) Math.round(angles[1] * 180.0 / Math.PI);
		double radius = measurement[0];
		int x1 = settings.mapX(cx - radius);
		int x2 = settings.mapX(cx + radius);
		int y1 = settings.mapY(cy + radius);
		int y2 = settings.mapY(cy - radius);
		g.setColor(settings.getSensorValueGraphicalColor());
		g.drawArc(x1, y1, x2 - x1, y2 - y1, a1, a2 - a1);
	}
	
	private static void drawRay(Graphics2D g, double[] measurement,
			Shape shape, double cx, double cy, VisualizationSettings settings) {
		double angles[] = shape.referenceAngles();
		double absAngle = measurement[0] + (angles[0] + angles[1])/2;
		double absAngleSin = Math.sin(absAngle);
		double absAngleCos= Math.cos(absAngle);
		double ranges[] = shape.referenceValues();
		int x1 = settings.mapX(cx + absAngleCos*ranges[0]);
		int x2 = settings.mapX(cx + absAngleCos*ranges[1]);
		int y1 = settings.mapY(cy + absAngleSin*ranges[0]);
		int y2 = settings.mapY(cy + absAngleSin*ranges[1]);
		g.setColor(settings.getSensorValueGraphicalColor());
		g.drawLine(x1, y1, x2, y2);
	}

	protected final static ShapeVisualisationVariant NO_VALUE_VISUALISATION = new ShapeVisualisationVariant("no values") {

		@Override
		public Color getBorderColor(double[] measurement, VisualizationSettings settings) {
			return settings.getSensorShapeBorder();
		}

		@Override
		public Color getFillColor(double[] measurement, VisualizationSettings settings) {
			return settings.getSensorShapeFill();
		}

		@Override
		public void additionalVisualisation(Graphics2D g, double[] measurement, Shape sensorShape, double cx, double cy, double r, double angle, VisualizationSettings settings) {
		}

	};

	protected final static ShapeVisualisationVariant TEXT_VISUALISATION = new ShapeVisualisationVariant("values as text") {

		@Override
		public Color getBorderColor(double[] measurement, VisualizationSettings settings) {
			return settings.getSensorShapeBorder();
		}

		@Override
		public Color getFillColor(double[] measurement, VisualizationSettings settings) {
			return settings.getSensorShapeFill();
		}

		@Override
		public void additionalVisualisation(Graphics2D g, double[] measurement, Shape shape, double cx, double cy, double r, double angle, VisualizationSettings settings) {
			double refPoint[] = shape.referencePoint();
			int x = settings.mapX(refPoint[0]);
			int y = settings.mapY(refPoint[1]);
			g.setColor(settings.getSensorValueTextColor());
			int numDigits = 2;
			drawMeasurement(g, x, y, measurement, numDigits);
		}
	};

	protected final static ShapeVisualisationVariant TEXT_VISUALISATION_IN_DISK = new ShapeVisualisationVariant("values in disk") {

		@Override
		public Color getBorderColor(double[] measurement, VisualizationSettings settings) {
			return settings.getSensorShapeBorder();
		}

		@Override
		public Color getFillColor(double[] measurement, VisualizationSettings settings) {
			return settings.getSensorShapeFill();
		}

		@Override
		public void additionalVisualisation(Graphics2D g, double[] measurement, Shape shape, double cx, double cy, double r, double angle, VisualizationSettings settings) {
			int x = settings.mapX(cx);
			int y = settings.mapY(cy);
			g.setColor(settings.getSensorValueTextColor());
			int numDigits = 2;
			drawMeasurement(g, x, y, measurement, numDigits);
		}
	};

	protected final static ShapeVisualisationVariant GRAPHICAL_ARC = new ShapeVisualisationVariant("graphical") {
		@Override
		public Color getBorderColor(double[] measurement, VisualizationSettings settings) {
			return settings.getSensorShapeBorder();
		}

		@Override
		public Color getFillColor(double[] measurement, VisualizationSettings settings) {
			return settings.getSensorShapeFill();
		}

		@Override
		public void additionalVisualisation(Graphics2D g, double[] measurement, Shape shape, double cx, double cy, double r, double angle, VisualizationSettings settings) {
			drawArc(g, measurement, shape, cx, cy, settings);
		}
	};
	
	
	protected final static ShapeVisualisationVariant GRAPHICAL_RAY = new ShapeVisualisationVariant("graphical") {
		@Override
		public Color getBorderColor(double[] measurement, VisualizationSettings settings) {
			return settings.getSensorShapeBorder();
		}

		@Override
		public Color getFillColor(double[] measurement, VisualizationSettings settings) {
			return settings.getSensorShapeFill();
		}

		@Override
		public void additionalVisualisation(Graphics2D g, double[] measurement, Shape shape, double cx, double cy, double r, double angle, VisualizationSettings settings) {
			drawRay(g, measurement, shape, cx, cy, settings);
		}
	};

	protected final static ShapeVisualisationVariant GRAPHICAL_RAY_TEXT = new ShapeVisualisationVariant("graphical+text") {
		@Override
		public Color getBorderColor(double[] measurement, VisualizationSettings settings) {
			return settings.getSensorShapeBorder();
		}

		@Override
		public Color getFillColor(double[] measurement, VisualizationSettings settings) {
			return settings.getSensorShapeFill();
		}

		@Override
		public void additionalVisualisation(Graphics2D g, double[] measurement, Shape shape, double cx, double cy, double r, double angle, VisualizationSettings settings) {
			drawRay(g, measurement, shape, cx, cy, settings);
			int x = settings.mapX(cx);
			int y = settings.mapY(cy);
			g.setColor(settings.getSensorValueTextColor());
			int numDigits = 2;
			drawMeasurement(g, x, y, measurement, numDigits);
		}
	};
	
	protected final static ShapeVisualisationVariant GRAPHICAL_ARC_TEXT = new ShapeVisualisationVariant("graphical+text") {
		@Override
		public Color getBorderColor(double[] measurement, VisualizationSettings settings) {
			return settings.getSensorShapeBorder();
		}

		@Override
		public Color getFillColor(double[] measurement, VisualizationSettings settings) {
			return settings.getSensorShapeFill();
		}

		@Override
		public void additionalVisualisation(Graphics2D g, double[] measurement, Shape shape, double cx, double cy, double r, double angle, VisualizationSettings settings) {
			drawArc(g, measurement, shape, cx, cy, settings);
			int x = settings.mapX(cx);
			int y = settings.mapY(cy);
			g.setColor(settings.getSensorValueTextColor());
			int numDigits = 2;
			drawMeasurement(g, x, y, measurement, numDigits);
		}
	};

}
