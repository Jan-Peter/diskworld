package diskworld.sensors;

import java.util.Set;

import diskworld.diskcomplexes.Disk;
import diskworld.diskcomplexes.DiskMaterial;
import diskworld.environment.Environment;

public class ColorSensitiveDirectionSensor extends ConeSensor {

	private static final double MIN_RANGE_RELATIVE = 1.5; // 50% more than disk radius
	private static final String SENSOR_NAME = "DiskDistance";
	private static final ShapeVisualisationVariant[] OPTIONS = new ShapeVisualisationVariant[] {
			GRAPHICAL_RAY_TEXT,
			GRAPHICAL_RAY,
			TEXT_VISUALISATION,
			TEXT_VISUALISATION_IN_DISK,
			NO_VALUE_VISUALISATION
	};

	private double maxRange;
	private double openingAngle;
	private Set<DiskMaterial> materials;

	public ColorSensitiveDirectionSensor(Environment environment, double centerAngle, double openingAngle, double maxRangeAbsolute, Set<DiskMaterial> materials) {
		super(environment, centerAngle, openingAngle, MIN_RANGE_RELATIVE, maxRangeAbsolute, SENSOR_NAME, OPTIONS);
		this.maxRange = maxRangeAbsolute;
		this.openingAngle = openingAngle;
		this.materials = materials;
	}

	@Override
	public double getMinValue() {
		return -openingAngle/2;
	}

	@Override
	public double getMaxValue() {
		return openingAngle/2;
	}

	@Override
	public int getDimension() {
		return 1;
	}

	@Override
	protected void doMeasurement(double measurement[]) {
		double centerx = getDisk().getX();
		double centery = getDisk().getY();
		double direction = getDisk().getAngle();
		double min = maxRange;
		double angle = 0;
		for (Disk d : getDisksInShape()) {
			DiskMaterial material = d.getDiskType().getMaterial();
			if(materials.contains(material)){
			double dx = d.getX() - centerx;
			double dy = d.getY() - centery;
			double dist = Math.sqrt(dx * dx + dy * dy) - d.getRadius();
			if (dist < min){
				min = dist;
				double alpha = Math.atan2(dy, dx);
				angle = alpha - direction;
			}
		}
		}
		measurement[0] = angle - 2*Math.PI * Math.round(angle/(2*Math.PI));
	}
}
