package diskworld.sensors;

import diskworld.shape.Shape;

/**
 * @author Jan
 * 
 *         Holds a template for shapes. It can generate shape when provided with the relevant disk parameters (coordinates, angle, radius).
 */
public interface ShapeTemplate {
	/**
	 * Generates a shape relative to the provided disk parameters.
	 * 
	 * @param centerx
	 *            x coordinate of disk centre
	 * @param centery
	 *            y coordinate of disk centre
	 * @param radius
	 *            radius of the disk
	 * @param angle
	 *            absolute orientation of the disk
	 * @return
	 *         a new Shape based on this template
	 */
	public Shape getShape(double centerx, double centery, double radius, double angle);
}
