package diskworld.sensors;

import diskworld.diskcomplexes.Disk;
import diskworld.interfaces.Sensor;
import diskworld.visualization.VisualisationItem;

/**
 * 
 * @author jph
 * 
 */
public class PlaceSensor implements Sensor {

	//default constructor
	public PlaceSensor() {
	}

	@Override
	public double getMinValue() {
		return Double.NEGATIVE_INFINITY;
	}

	@Override
	public double getMaxValue() {
		return Double.POSITIVE_INFINITY;
	}

	@Override
	public int getDimension() {
		return 2;
	}

	@Override
	/**
	 * @see diskworld.interfaces.Sensor#getMeasurement(diskworld.diskcomplexes.Disk, double[])
	 * double[] is interpreted as 2D-Array of position (x,y)
	 */
	public void doMeasurement(Disk disk, double pos[]) {
		pos[0] = disk.getX();
		pos[1] = disk.getY();
	}

	@Override
	public VisualisationItem getVisualisationItem() {
		// TODO Auto-generated method stub
		return null;
	}

}
