package diskworld.shape;

import diskworld.environment.Wall;

public class WallShape extends ArbitraryShape {

	public WallShape(Wall wall) {
		super(getWallBoundary(wall));
	}

	private static BoundaryElement[] getWallBoundary(Wall wall) {
		double x1 = wall.getX1();
		double y1 = wall.getY1();
		double x2 = wall.getX2();
		double y2 = wall.getY2();
		double dx = wall.getHalfThicknessX();
		double dy = wall.getHalfThicknessY();
		double r = wall.getHalfThicknessX();
		double angle = Math.atan2(dy, dx);
		return new BoundaryElement[] { 
				new LineBoundary(x1-dx, y1-dy, x2-dy, y2-dy),
				new ArcBoundary(x2, y2, r, angle-Math.PI, angle),
				new LineBoundary(x2+dx, y2+dy, x1+dy, y1+dy),
				new ArcBoundary(x1, y1, r, angle, angle+Math.PI)			
		};
	}

}
