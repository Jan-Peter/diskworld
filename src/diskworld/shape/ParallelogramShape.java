package diskworld.shape;

public class ParallelogramShape extends ArbitraryShape {

	private final double x,y;
	private final double dx1,dy1;
	private final double dx2,dy2;

	public ParallelogramShape(double x, double y, double dx1, double dy1, double dx2, double dy2) {
		super(new BoundaryElement[] { 
				new LineBoundary(x, y, x+dx1, y+dy1),
				new LineBoundary(x+dx1, y+dy1, x+dx1+dx2, y+dy1+dy2),
				new LineBoundary(x+dx1+dx2, y+dy1+dy2, x+dx2, y+dy2),
				new LineBoundary(x+dx2, y+dy2, x, y)
		});
		this.x = x;
		this.y = y;
		this.dx1 = dx1;
		this.dy1 = dy1;
		this.dx2 = dx2;
		this.dy2 = dy2;
	}

	public double getx() {
		return x;
	}

	public double gety() {
		return y;
	}
	
	public double getdx1() {
		return dx1;
	}

	public double getdy1() {
		return dy1;
	}

	public double getdx2() {
		return dx2;
	}

	public double getdy2() {
		return dy2;
	}

}
