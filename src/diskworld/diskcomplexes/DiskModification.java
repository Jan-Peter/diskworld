package diskworld.diskcomplexes;

/**
 * Encapsulates the modification of a disk (in a DiskComplexEnsemble).
 * This may involve x,y coordinates, rotation angle or radius.
 * Values that shall not be changed are marked with NO_CHANGE 
 */
public class DiskModification {
	private final Disk disk;
	private final double x,y;
	private final double angle;
	private final double radius;
	
	private static final double NO_CHANGE = Double.NaN;
	
	/**
	 * Constructor specifying changes to a disk.
	 * 
	 * @param disk the disk to be changed
	 * @param x the new absolute x coordinate
	 * @param y the new absolute y coordinate
	 * @param angle the new absolute angle
	 * @param radius the new radius
	 */
	public DiskModification(Disk disk, double x, double y, double angle, double radius) {
		this.disk = disk;
		this.x = x;
		this.y = y;
		this.angle = angle;
		this.radius = radius;
	}

	/**
	 * Convenience constructor letting the radius unchanged.
	 * 
	 * @param disk the disk to be changed
	 * @param x the new absolute x coordinate
	 * @param y the new absolute y coordinate
	 * @param angle the new absolute angle
	 */
	public DiskModification(Disk disk, double x, double y, double angle) {
		this(disk,x,y,angle,NO_CHANGE);
	}

	/**
	 * This performs the modification. Is not public and should be only called from the 
	 * DiskComplexEnsemble to which the changed disk belongs.
	 */
	void modify() {
		if (changesPosition()) {
			disk.setPosition(x, y);
		}
		if (changesAngle()) {
			disk.setAngle(angle);
		}
		if (changesRadius()) {
			disk.setRadius(radius);
		}
	}

	public Disk getDisk() {
		return disk;
	}

	public boolean changesPosition() {
		return !(Double.isNaN(x) || Double.isNaN(y));
	}

	public boolean changesAngle() {
		return !Double.isNaN(angle);
	}
	
	public boolean changesRadius() {
		return !Double.isNaN(radius);
	}
	
}
