package diskworld.diskcomplexes;

import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.interfaces.Sensor;
import diskworld.linalg2D.RelativePosition;

public class Disk implements CollidableObject {

	/**
	 * Current position relative to DiskComplex
	 */
	private final RelativePosition position;

	/**
	 * Current activation in case the disk is attached to an actuator, otherwise only used for visualisation
	 */
	private double activity;

	/**
	 * Latest measurements of the sensors attached to this disk (null if no sensors present)
	 */
	private final double[][] measurement;

	/**
	 * Current radius, may change during simulation (due to growing, shrinking)!
	 */
	private double radius;

	/**
	 * Current mass, may change during simulation (due to growing, shrinking)!
	 */
	private double mass;

	/**
	 * Static properties (that are not supposed to change during the lifetime of a simulation)
	 */
	private final DiskType diskType;

	/**
	 * Link to the DiskComplex the disk belongs to
	 */
	private DiskComplex belongsTo;

	/**
	 * Handler called when collision of this disk occurs
	 */
	private CollisionEventHandler eventHandler;

	/**
	 * Non-public constructor
	 * 
	 * @param diskComplex
	 *            the DiskComplex to which the new Disk will be added to
	 * @param x
	 *            current absolute x position
	 * @param y
	 *            current absolute y position
	 * @param radius
	 *            radius of the disk
	 * @param angle
	 *            rotation angle of the disk
	 * @param diskType
	 *            type of the disk
	 */
	Disk(DiskComplex diskComplex, double x, double y, double radius, double angle, DiskType diskType) {
		belongsTo = diskComplex;
		position = new RelativePosition(belongsTo.getCoordinates());
		position.setAbsPosition(x, y);
		position.setAbsAngle(angle);
		this.diskType = diskType;
		eventHandler = null;
		setRadius(radius); // computes the mass 
		setActivity(diskType.hasActuator() ? diskType.getActuator().getInitialActivity() : 0.0);
		measurement = createMeasurmentArays(diskType.getSensors());
	}

	/**
	 * Provide an EventHandler (disables an already existing EventHandler!)
	 * 
	 * @param handler
	 *            new CollisionEventHandler
	 */
	public void setEventHandler(CollisionEventHandler handler) {
		eventHandler = handler;
	}

	/**
	 * Get the currently installed EventHandler
	 * 
	 * @return current EventHandler or null
	 */
	public CollisionEventHandler getEventHandler() {
		return eventHandler;
	}

	/**
	 * Current absolute x position
	 * 
	 * @return current absolute x
	 */
	public double getX() {
		return position.getAbsX();
	}

	/**
	 * Current absolute y position
	 * 
	 * @return current absolute y
	 */
	public double getY() {
		return position.getAbsY();
	}

	/**
	 * The type of this disk (may be shared with other Disk objects)
	 * 
	 * @return static properties of this disk
	 */
	public DiskType getDiskType() {
		return diskType;
	}

	/**
	 * Current absolute angle
	 * 
	 * @return current absolute rotation angle (0-2*Pi, east = 0, mathematically positive)
	 */
	public double getAngle() {
		return position.getAbsAngle();
	}

	/**
	 * Current current radius
	 * 
	 * @return current radius
	 */
	public double getRadius() {
		return radius;
	}

	/**
	 * Current current mass
	 * 
	 * @return current mass
	 */
	public double getMass() {
		return mass;
	}

	/**
	 * Current "activation" of the Disk, only relevant if an Actuator is attached to this Disk's DiskType
	 * 
	 * @return current activation value
	 */
	public double getActivity() {
		return activity;
	}

	/**
	 * Set "activation" of the Disk, only relevant if an Actuator is attached to this Disk's DiskType
	 * 
	 * @param activity
	 *            new activity value
	 */
	public void setActivity(double activity) {
		this.activity = activity;
	}

	/**
	 * Get the array sensor values
	 * 
	 * @return double 2D array that the sensor measurements are stored in
	 */
	public double[][] getSensorMeasurements() {
		return measurement;
	}

	void shiftPosition(double deltax, double deltay) {
		position.setAbsPosition(position.getAbsX() + deltax, position.getAbsY() + deltay);
	}

	double getDistanceToOrigin() {
		return position.getDistanceToOrigin();
	}

	void changeOwner(DiskComplex newOwner) {
		belongsTo = newOwner;
		position.changeCoordinates(newOwner.getCoordinates());
	}

	/**
	 * Does the disk overlap with another disk
	 * 
	 * @param other
	 *            the other disk
	 * @return true if the two overlap
	 */
	public boolean intersects(Disk other) {
		return intersectsDisk(other.getX(), other.getY(), other.getRadius());
	}

	private boolean intersectsDisk(double x2, double y2, double r2) {
		return Collision.intersecting(getX(), getY(), radius, x2, y2, r2);
	}

	/**
	 * Does another disk belong to the same DiskComplex or another?
	 * 
	 * @param other
	 *            the other Disk
	 * @return true if other disk belongs to the same as this
	 */
	public boolean belongsToSame(Disk other) {
		return belongsTo == other.belongsTo;
	}

	/**
	 * Does this disk belong (currently) to the given DiskComplex?
	 * 
	 * @param diskComplex
	 *            the diskComplex to test
	 * @return true if this belongs to the given DiskComplex
	 */
	public boolean belongsTo(DiskComplex diskComplex) {
		return belongsTo == diskComplex;
	}

	/**
	 * The DiskComplex to which this disk belongs to (currently)
	 * 
	 * @return current DiskComplex
	 */
	public DiskComplex getDiskComplex() {
		return belongsTo;
	}

	//  Modifications:

	/**
	 * Set absolute position. This method is not public, it should never be called directly! Use DiskModification to change a disks position!
	 * 
	 * @param absPosx
	 *            new absolute x coordinate
	 * @param absPosy
	 *            new absolute y coordinate
	 */
	void setPosition(double absPosx, double absPosy) {
		position.setAbsPosition(absPosx, absPosy);
	}

	/**
	 * Set absolute angle. This method is not public, it should never be called directly! Use DiskModification to change a disks angle!
	 * 
	 * @param absAngle
	 *            new absolute angle coordinate
	 */
	void setAngle(double absAngle) {
		position.setAbsAngle(absAngle);
	}

	/**
	 * Set radius. Beware:This method changes the mass! This method is not public, it should never be called directly! Use DiskModification to change a disks radius!
	 * 
	 * @param radius
	 *            new value for radius (must be positive)
	 */
	void setRadius(double radius) {
		if (radius <= 0.0)
			throw new IllegalArgumentException("Radius must be positive");
		this.radius = radius;
		mass = Math.PI * radius * radius * diskType.getMaterial().getDensity();
	}

	private double[][] createMeasurmentArays(Sensor[] sensors) {
		if (sensors == null)
			return null;
		double[][] res = new double[sensors.length][];
		for (int i = 0; i < sensors.length; i++) {
			res[i] = new double[sensors[i].getDimension()];
		}
		return res;
	}

}
