package diskworld.diskcomplexes;

/**
 * Encapsulates material properties needed for Disk: 
 *  - density
 *  - color
 *  - elasticity
 *  - friction
 */
public class DiskMaterial {

	private final double density;
	private final int defaultColor;
	
	public DiskMaterial(double density, int defaultColor) {
		this.density = density;
		this.defaultColor = defaultColor;
	}
	
	/**
	 * Provides the density, used to calculate the mass of disks
	 * 
	 * @return positive number
	 */
	public double getDensity() {
		return density;
	}

	/**
	 * Provides the default color for the material
	 * 
	 * @return color index (>= 0)
	 */
	public int getDefaultColor() {
		return defaultColor;
	}

}
