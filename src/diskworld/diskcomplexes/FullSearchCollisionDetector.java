package diskworld.diskcomplexes;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

import diskworld.environment.Wall;
import diskworld.interfaces.CollisionDetector;
import diskworld.interfaces.DiskChangeListener;

public class FullSearchCollisionDetector implements CollisionDetector, DiskChangeListener {

	private Set<Disk> disks;
	private Collection<Wall> walls;

	public FullSearchCollisionDetector(LinkedList<Wall> walls) {
		disks = new HashSet<Disk>();
		this.walls = walls;
	}

	@Override
	public void diskHasMoved(Disk d) {
		// nothing to do
	}

	@Override
	public void diskWasAdded(Disk d) {
		disks.add(d);
	}

	@Override
	public void diskWasRemoved(Disk d) {
		disks.remove(d);
	}

	@Override
	public void diskHasChangedRadius(Disk d) {
		// nothing to do
	}

	@Override
	public LinkedList<Collision> getNonSelfCollisions(DiskComplex dc) {
		LinkedList<Collision> res = new LinkedList<Collision>();
		for (Iterator<Disk> i = dc.getDisks().iterator(); i.hasNext();) {
			Disk d1 = i.next();
			for (Iterator<Disk> j = disks.iterator(); j.hasNext();) {
				Disk d2 = j.next();
				if (!d2.belongsTo(dc)) {
					Collision c = Collision.diskCollision(d1, d2);
					if (c != null) {
						res.add(c);
					}
				}
			}
			for (Wall wall : walls) {
				Collision c = Collision.wallCollision(d1, wall);
				if (c != null) {
					res.add(c);
				}
			}
		}
		return res;
	}

	@Override
	public LinkedList<Collision> getNonSelfCollisions() {
		LinkedList<Collision> res = new LinkedList<Collision>();
		for (Iterator<Disk> i = disks.iterator(); i.hasNext();) {
			Disk d1 = i.next();
			boolean cont = true;
			for (Iterator<Disk> j = disks.iterator(); cont && j.hasNext();) {
				Disk d2 = j.next();
				if (d2 == d1) {
					cont = false;
				} else {
					if (!d1.belongsToSame(d2)) {
						Collision c = Collision.diskCollision(d1, d2);
						if (c != null) {
							res.add(c);
						}
					}
				}
			}
			for (Wall wall : walls) {
				Collision c = Collision.wallCollision(d1, wall);
				if (c != null) {
					res.add(c);
				}
			}
		}
		return res;
	}

	@Override
	public boolean hasSelfCollisions(DiskComplex dc) {
		for (Iterator<Disk> i = dc.getDisks().iterator(); i.hasNext();) {
			Disk d1 = i.next();
			boolean cont = true;
			for (Iterator<Disk> j = dc.getDisks().iterator(); j.hasNext() && cont;) {
				Disk d2 = j.next();
				if (d1 != d2) {
					Collision c = Collision.diskCollision(d1, d2);
					if (c != null) {
						return true;
					}
				} else {
					cont = false;
				}
			}
		}
		return false;
	}

}
