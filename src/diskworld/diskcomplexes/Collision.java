package diskworld.diskcomplexes;

import diskworld.environment.PhysicsParameters;
import diskworld.environment.Wall;
import diskworld.interfaces.CollidableObject;
import diskworld.linalg2D.Point;

public class Collision {
	// TODO: Remove Epsilons!
	public static final double EPSILON_SPEED_DIFF = 1e-7;
	public static final double EPSILON_WALL_COLLISION = 1e-7;
	public static final double EPSILON_DISK_INTERSECT = 1e-7;
	private final Disk obj1;
	private final CollidableObject obj2; // disk or wall
	private final double collisionPointx,collisionPointy; // touching point 
	private final double directionx,directiony; // direction from disk1 to disk2, or along the normal in case of wall, length 1
	private double relativeSpeedByEgoMotion; // relative speed that is supposed to be added to the speed difference imposed by DiskComplex
	
	/**
	 * Constructor for a Collision-Object
	 * @param x1 x-coordinate 
	 * @param y1 y-coordinate
	 * @param dx distance in x-direction
	 * @param dy distance in y-direction
	 * @param obj1 Disk colliding
	 * @param obj2 disk or wall obj1 is colliding with
	 * @param factorCollisionPoint set between 0.0 and 1.0, defines the collision point along the distance vector
	 */
	private Collision(double x1, double y1, double dx, double dy, Disk obj1, CollidableObject obj2, double factorCollisionPoint) {
		this.obj1 = obj1;
		this.obj2 = obj2;
		collisionPointx = x1 + dx*factorCollisionPoint;
		collisionPointy = y1 + dy*factorCollisionPoint;
		double f = 1/Math.sqrt(dx*dx+dy*dy);
		directionx = f*dx;
		directiony = f*dy;
		relativeSpeedByEgoMotion = 0.0;
	}

	/**
	 * Constructor for Collision, calculating the factorCollisionPoint from radii and distances from the coordinates
	 * @param disk1 colliding disk
	 * @param x1 x-position disk1
	 * @param y1 y-position disk1
	 * @param r1 radius disk1
	 * @param obj2 disk or wall disk1 collides with (has to have radius - be round)
	 * @param x2 x-position obj2
	 * @param y2 y-position obj2
	 * @param r2 radius obj2
	 */
	private Collision(Disk disk1, double x1, double y1, double r1, CollidableObject obj2, double x2, double y2, double r2) {
		this(x1,y1,x2-x1,y2-y1,disk1,obj2,r1/(r1+r2));
	}

	/**
	 * Collision of two disks
	 * @param disk1
	 * @param disk2
	 * @return Collision-Object if intersecting, null otherwise
	 */
	public static Collision diskCollision(Disk disk1, Disk disk2) {
		return roundObjectCollision(disk1,disk1.getX(),disk1.getY(),disk1.getRadius(),disk2,disk2.getX(),disk2.getY(),disk2.getRadius());
	}

	/**
	 * Calulates Collison for round objects
	 * @param disk1 colliding disk
	 * @param x1 x-position disk1
	 * @param y1 y-position disk1
	 * @param r1 radius disk1
	 * @param obj2 disk or wall disk1 collides with (has to have radius - be round)
	 * @param x2 x-position obj2
	 * @param y2 y-position obj2
	 * @param r2 radius obj2
	 * @return Collision-Object if intersecting, null otherwise
	 */
	public static Collision roundObjectCollision(Disk disk1, double x1, double y1, double r1, CollidableObject obj2, double x2, double y2, double r2) {
		if (Collision.intersecting(x1, y1, r1, x2, y2, r2)) 
			return new Collision(disk1, x1, y1, r1, obj2, x2, y2, r2);
		else
			return null;
	}

	/**
	 * Calls {@link #roundObjectCollision(Disk, double, double, double, CollidableObject, double, double, double)} reading the parameters from disk1
	 * @return Collision-Object if intersecting, null otherwise
	 */
	private static Collision roundObjectCollision(Disk disk1, CollidableObject obj2, double x2, double y2, double r2) {
		return roundObjectCollision(disk1, disk1.getX(), disk1.getY(), disk1.getRadius(), obj2, x2, y2, r2);
	}

	/**
	 * Calculates Collision disk to wall
	 * @param disk
	 * @param wall
	 * @return Collision-Object if intersecting, null otherwise
	 */
	public static Collision wallCollision(Disk disk, Wall wall) {
		Collision res;
		double dx = wall.getHalfThicknessX();
		double dy = wall.getHalfThicknessY();
		for (int i = 0; i < 2; i++) {
			res = lineCollision(disk, wall, wall.getX1()+dx, wall.getY1()+dy, wall.getX2()+dx, wall.getY2()+dy);
			if (res != null)
				return res;
			dx *= -1;
			dy *= -1;
		}
		double d = wall.getHalfThickness();
		res = roundObjectCollision(disk, wall, wall.getX1(), wall.getY1(), d);
		if (res != null)
			return res;
		res = roundObjectCollision(disk, wall, wall.getX2(), wall.getY2(), d);
		return res;
	}

	public void setEgoMotion(double egoMotionRelativeSpeed) {
		relativeSpeedByEgoMotion = egoMotionRelativeSpeed;
	}
	
	/**
	 * Calculates Collision disk to wall (line piece)
	 * @return Collision-Object if intersecting, null otherwise
	 */
	private static Collision lineCollision(Disk disk, Wall wall, double x1, double y1, double x2, double y2) {
		double dx = x2-x1;
		double dy = y2-y1;
		double vx = disk.getX()-x1;
		double vy = disk.getY()-y1;
		double proj = vx*dx + vy*dy;
		double f = proj/(dx*dx+dy*dy);
		if ((f < 0.0) || (f > 1.0))
			return null;
		double ox = -vx + dx*f;
		double oy = -vy + dy*f;
		double r = disk.getRadius()-EPSILON_WALL_COLLISION;
		if (ox*ox+oy*oy <= r*r) {
			return new Collision(disk.getX(), disk.getY(),ox,oy,disk,wall,1.0);
		}	
		return null;
	}

	/**
	 * Checks whether there is an exchange impulse and applies if necessary
	 * @param elasticity
	 * @return true if impulse is applied, false else
	 */
	public boolean exchangeImpulse(double elasticity) {
		DiskComplex dc1 = obj1.getDiskComplex();
		DiskComplex dc2 = obj2 instanceof Disk ? ((Disk)obj2).getDiskComplex() : null;
		if(dc1.isStatical() && (dc2 == null || dc2.isStatical()))
			return false;
		double speed1,speed2;
		speed1 = dc1.getProjectedSpeed(collisionPointx, collisionPointy, directionx, directiony);
		speed2 = dc2 == null ? 0.0 : dc2.getProjectedSpeed(collisionPointx, collisionPointy, directionx, directiony);
		double speedDiff = speed1 - speed2 + relativeSpeedByEgoMotion; 
		//System.out.println(speed1 +","+ speed2 +","+ relativeSpeedByEgoMotion);
		relativeSpeedByEgoMotion = 0;
		if (speedDiff > EPSILON_SPEED_DIFF) {
			double change1,change2;
			change1 = dc1.getProjectedSpeedChangeByImpulse(collisionPointx, collisionPointy, directionx, directiony);
			change2 = dc2 == null ? 0.0 : dc2.getProjectedSpeedChangeByImpulse(collisionPointx, collisionPointy, directionx, directiony);
			//System.out.println("Exchange: "+change1+","+change2);
			double factor = -speedDiff/(change1+change2)*(1.0+elasticity);
			double impulsex = directionx*factor; 
			double impulsey = directiony*factor;
			if(!dc1.isStatical())
				dc1.applyImpulse(impulsex, impulsey, collisionPointx, collisionPointy);
			if (dc2 != null && !dc2.isStatical()) 
				dc2.applyImpulse(-impulsex, -impulsey, collisionPointx, collisionPointy);
			return true;
		} else {
			return false;
		}
	}

	public double getElasticity(PhysicsParameters param) {
		return obj2 instanceof Disk  ? param.getDisk2DiskElasticty(obj1.getDiskType().getMaterial(), ((Disk)obj2).getDiskType().getMaterial()) :
									   param.getDisk2WallElasticty(obj1.getDiskType().getMaterial()); 
	}

	public Point getCollisionPoint() {
		return new Point(collisionPointx,collisionPointy);
	}

	public Disk getObj1() {
		return obj1;
	}

	public CollidableObject getObj2() {
		return obj2;
	}

	public double getProjection(double dx, double dy) {
		return dx*directionx+dy*directiony;
	}

	public static boolean intersecting(double x1, double y1, double r1, double x2, double y2, double r2) {
		double dx = x2-x1;
		double dy = y2-y1;
		double sr = r1+r2-Collision.EPSILON_DISK_INTERSECT;
		return dx*dx+dy*dy <= sr*sr;
	}

}
