package diskworld.diskcomplexes;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import diskworld.environment.Floor;
import diskworld.environment.PhysicsParameters;
import diskworld.interfaces.CollidableObject;
import diskworld.interfaces.CollisionDetector;
import diskworld.interfaces.CollisionEventHandler;
import diskworld.interfaces.DiskChangeListener;

/**
 * @author jan
 * 
 */
public class DiskComplexEnsemble {

	private final Set<DiskComplex> diskComplexes;
	private final Set<DiskChangeListener> changeListeners;
	private final CollisionDetector collisionDetector;
	private final Set<Disk> sensorDisks;
	private final Set<Disk> actuatorDisks;

	public DiskComplexEnsemble(CollisionDetector collisionDetector, Floor floor) {
		diskComplexes = new HashSet<DiskComplex>();
		this.collisionDetector = collisionDetector;
		changeListeners = new HashSet<DiskChangeListener>();
		sensorDisks = new HashSet<Disk>();
		actuatorDisks = new HashSet<Disk>();
	}

	public void addChangeListener(DiskChangeListener changeListener) {
		changeListeners.add(changeListener);
	}

	public void removeChangeListener(DiskChangeListener changeListener) {
		changeListeners.remove(changeListener);
	}

	/*	public void clear() {
			for (DiskComplex dc : diskComplexes) {
				if (!dc.isMerged())
					for (DiskChangeListener changeListener : changeListeners) {
						for (Disk d : dc.getDisks()) {
							changeListener.diskWasRemoved(d);
						}
					}
			}
			collisionDetector.setWalls(null);
			diskComplexes.clear();
		}*/

	public Collection<DiskComplex> getDiskComplexes() {
		return diskComplexes;
	}

	/**
	 * Provides all disks that have sensors
	 * 
	 * @return collection of sensor disks
	 */
	public Collection<Disk> getSensorDisks() {
		return sensorDisks;
	}

	/**
	 * Provides all disks that have an actuator
	 * 
	 * @return collection of actuator disks
	 */
	public Collection<Disk> getActuatorDisks() {
		return actuatorDisks;
	}

	public DiskComplex createNewDiskComplex() {
		DiskComplex res = new DiskComplex();
		diskComplexes.add(res);
		return res;
	}

	public Disk addNewDisk(DiskComplex dc, double x, double y, double radius, double angle, DiskType diskType) {
		Disk res = new Disk(dc, x, y, radius, angle, diskType);
		dc.addNewDisk(res);
		for (DiskChangeListener changeListener : changeListeners) {
			changeListener.diskWasAdded(res);
		}
		if (diskType.hasSensors()) {
			sensorDisks.add(res);
		}
		if (diskType.hasActuator()) {
			actuatorDisks.add(res);
		}
		return res;
	}

	public void removeDiskComplex(DiskComplex dc) {
		if (diskComplexes.remove(dc)) {
			for (Disk d : dc.getDisks()) {
				for (DiskChangeListener changeListener : changeListeners) {
					changeListener.diskWasRemoved(d);
				}
				DiskType diskType = d.getDiskType();
				if (diskType.hasSensors()) {
					sensorDisks.remove(d);
				}
				if (diskType.hasActuator()) {
					actuatorDisks.remove(d);
				}
			}
		}
	}

	public void exchangeImpulsesInCollisions(List<Collision> collisions, PhysicsParameters physicsParameters) {
		if (!collisions.isEmpty())
			System.out.println(collisions.size() + " collision found");
		boolean changed = false;
		for (Collision c : collisions) {
			// System.out.println(" Collision first evaluation");
			if (c.exchangeImpulse(c.getElasticity(physicsParameters)))
				changed = true;
		}
		int num = 0;
		while (changed) {
			num++;
			changed = false;
			for (Collision c : collisions) {
				// System.out.println(" Collision iterted evaluation "+num);
				if (c.exchangeImpulse(0.0))
					changed = true;
			}
		}
		num--;
		if (num > 0)
			System.out.println(num + " additional collision resolution iterations done");
	}

	public void callCollisionEventHandlers(List<Collision> collisions) {
		for (Collision c : collisions) {
			Disk obj1 = c.getObj1();
			CollidableObject obj2 = c.getObj2();
			callEventHandler(obj1, obj2, c);
			if (obj2 instanceof Disk)
				callEventHandler((Disk) obj2, obj1, c);
		}
	}

	private void callEventHandler(Disk disk, CollidableObject obj2, Collision c) {
		CollisionEventHandler handler = disk.getEventHandler();
		if (handler != null) {
			handler.collision(obj2, c.getCollisionPoint());
		}
	}

	/**
	 * Perform a time step
	 * 
	 * @param dt
	 *            the delta that time shall advances
	 * @param physicsParameters
	 */
	public void doTimeStep(double dt, PhysicsParameters physicsParameters) {
		for (DiskComplex dc : diskComplexes) {
			if (!dc.isMerged()) {
				if (dc.doTimeStep(dt, physicsParameters.getFrictionModel())) {
					for (DiskChangeListener changeListener : changeListeners) {
						for (Disk d : dc.getDisks()) {
							changeListener.diskHasMoved(d);
						}
					}
				}
			}
		}
		LinkedList<Collision> collisions = collisionDetector.getNonSelfCollisions();
		exchangeImpulsesInCollisions(collisions, physicsParameters);
		callCollisionEventHandlers(collisions);
	}

	//	/**
	//	 * Set the absolute positions of a set of disks (belonging to specified disk complex) unless there are self collisions
	//	 * 
	//	 * @param newPosMap map from Disks to double[3] containing the new x-coordinate,y-coordinate,angle
	//	 * @return true if there were no collisions (between moved disks, other disks, walls) and the position change actually took place
	//	 */
	//	public boolean setPositionsAndAnglesIfNotSelfColliding(DiskComplex rootDiskComplex, Map<Disk,double[]> newPosMap) {
	//		swapPositionsAndAngles(newPosMap);
	//		if (neighborhoodTable.hasSelfCollisions(rootDiskComplex)) {
	//			swapPositionsAndAngles(newPosMap);
	//			return false;
	//		}
	//		return true;
	//	}

	//	private void swapPositionsAndAngles(Map<Disk,double[]> newPosMap) {
	//		for (Entry<Disk, double[]> e : newPosMap.entrySet()) {
	//			Disk d = e.getKey();
	//			d.swapPositionAndAngle(e.getValue());
	//			neighborhoodTable.diskHasMoved(d);
	//		}
	//	}

	public double getMaxTimeStep() {
		double min = Double.MAX_VALUE;
		for (DiskComplex dc : diskComplexes) {
			if (!dc.isMerged())
				min = Math.min(min, dc.getMaxTimeStep());
		}
		return min;
	}

	public CollisionDetector getCollisionDetector() {
		return collisionDetector;
	}

	/**
	 * Perform modification of disks
	 */
	public void performDiskModification(DiskModification modification) {
		modification.modify();
		Disk d = modification.getDisk();
		if (modification.changesPosition()) {
			for (DiskChangeListener changeListener : changeListeners) {
				changeListener.diskHasMoved(d);
			}
		}
		if (modification.changesRadius()) {
			for (DiskChangeListener changeListener : changeListeners) {
				changeListener.diskHasChangedRadius(d);
			}
		}

	}

}
