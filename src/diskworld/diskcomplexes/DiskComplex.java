package diskworld.diskcomplexes;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import diskworld.interfaces.FrictionModel;
import diskworld.linalg2D.CoordinateSystem;
import diskworld.visualization.Log;

// Holds a set of disks. The center of mass of the relative positions must be 0,0 at all times.

/**
 * Holds a set of disks. There is a CoordinateSystem associated with a DiskComplex, the origin of which is given by the center of mass.
 */
public class DiskComplex {
	private final List<Disk> disks;
	private final CoordinateSystem coordinates;
	private double mass, massMomentum, massRadiusSum;
	private double momentumx, momentumy, angularMomentum;
	private double speedx, speedy, angularSpeed;
	private double minDiskRadius;
	private double minDiskRadiusOverDistanceToOrigin;
	private DiskComplex mergedTo;
	private boolean statical = false;
	//private boolean impulseApplied;

	public static final double MAX_RELATIVE_DISK_TIMESTEP_DISPLACEMENT = 0.10; // max. 10% of radius displacement per time step

	public DiskComplex() {
		disks = new LinkedList<Disk>();
		coordinates = new CoordinateSystem();
		mass = 0.0;
		massMomentum = 0.0;
		massRadiusSum = 0.0;
		momentumx = 0.0;
		momentumy = 0.0;
		angularMomentum = 0.0;
		speedx = 0.0;
		speedy = 0.0;
		angularSpeed = 0.0;
		minDiskRadius = 0.0;
		minDiskRadiusOverDistanceToOrigin = 0.0;
		mergedTo = null;
		//collisions = new HashMap<Disk,Map<CollidableObject,Collision>>();
	}

	public boolean isMerged() {
		return mergedTo != null;
	}

	public double getSpeedx() {
		return speedx;
	}

	public double getSpeedy() {
		return speedy;
	}

	public double getAngularSpeed() {
		return angularSpeed;
	}

	public double getMass() {
		return mass;
	}

	public double getCenterx() {
		return coordinates.getOriginX();
	}

	public double getCentery() {
		return coordinates.getOriginY();
	}

	public Collection<Disk> getDisks() {
		return disks;
	}

	public CoordinateSystem getCoordinates() {
		return coordinates;
	}

	//	public boolean wasImpulseApplied() {
	//		boolean ret = impulseApplied;
	//		impulseApplied = false;
	//		return ret; 
	//	}

	/**
	 * x component of speed of a point that moves and rotates with this DiskComplex
	 * 
	 * @param x
	 *            absolute x coordinate of point
	 * @param y
	 *            absolute y coordinate of point
	 * @return x component of speed of point
	 */
	public double getSpeedx(double x, double y) {
		return getSpeedx() - (y - getCentery()) * getAngularSpeed();
	}

	/**
	 * y component of speed of a point that moves and rotates with this DiskComplex
	 * 
	 * @param x
	 *            absolute x coordinate of point
	 * @param y
	 *            absolute y coordinate of point
	 * @return y component of speed of point
	 */
	public double getSpeedy(double x, double y) {
		return getSpeedy() + (x - getCenterx()) * getAngularSpeed();
	}

	/**
	 * x component of speed of disk that moves and rotates with this DiskComplex
	 * 
	 * @param disk
	 *            disk
	 * @return x component of speed of point
	 */
	public double getSpeedx(Disk disk) {
		return getSpeedx(disk.getX(), disk.getY());
	}

	/**
	 * y component of speed of disk that moves and rotates with this DiskComplex
	 * 
	 * @param disk
	 *            disk
	 * @return y component of speed of point
	 */
	public double getSpeedy(Disk disk) {
		return getSpeedy(disk.getX(), disk.getY());
	}

	/**
	 * Perform a time step
	 * 
	 * @param dt
	 *            the delta that time shall advance
	 * @param frictionModel
	 *            Model for Friction coefficients
	 * @return true if the disk complex has moved
	 */
	public boolean doTimeStep(double dt, FrictionModel frictionModel) {
		if (isMerged()) {
			Log.abort("cannot do time step for merged complexes");
		}
		if (frictionModel != null) {
			if (isMoving()) {
				applyFriction(dt, frictionModel);
			}
		}
		if (isMoving()) {
			//System.out.print(coordinates.getOriginX()+"+"+getSpeedx()+"*"+dt+" = ");
			//System.out.print(coordinates.getAngle()+"+"+getAngularSpeed()+"*"+dt+" = ");
			double newx = coordinates.getOriginX() + getSpeedx() * dt;
			double newy = coordinates.getOriginY() + getSpeedy() * dt;
			double newangle = coordinates.getAngle() + getAngularSpeed() * dt;
			coordinates.setOrigin(newx, newy);
			coordinates.setAngle(newangle);
			//System.out.println(coordinates.getAngle());
			return true;
		} else {
			return false;
		}
	}

	private void applyFriction(double dt, FrictionModel frictionModel) {
		double energie = getTotalEnergy();
		applyAllFrictionImpulses(dt, frictionModel);
		reCalculateSpeeds();
		if (getTotalEnergy() > energie) {
			momentumx = 0.0;
			momentumy = 0.0;
			angularMomentum = 0.0;
			reCalculateSpeeds();
		}
	}

	public double getTotalEnergy() {
		return 0.5 * (mass * (speedx * speedx + speedy * speedy) + massMomentum * angularSpeed * angularSpeed);
	}

	private void applyAllFrictionImpulses(double dt, FrictionModel frictionModel) {
		if (frictionModel.frictionIsGlobal()) {
			double sx = getSpeedx();
			double sy = getSpeedy();
			double s2 = sx * sx + sy * sy;
			if (s2 > 0) {
				double f = dt / Math.sqrt(s2) * getMass() * frictionModel.getGlobalFrictionCoefficient();
				momentumx -= f * sx;
				momentumy -= f * sy;
			}
			angularMomentum -= dt * massRadiusSum * frictionModel.getGlobalFrictionCoefficient() * Math.signum(angularMomentum);
		} else {
			for (Disk d : disks) {
				applyFrictionImpulse(d, getSpeedx(d), getSpeedy(d), dt, frictionModel);
			}
		}
	}

	private void applyFrictionImpulse(Disk d, double speedx, double speedy, double dt, FrictionModel frictionModel) {
		double s2 = speedx * speedx + speedy * speedy;
		if (s2 > 0) {
			//double f = dt/Math.sqrt(s2)*d.getFloorContact(physicsParameters.getFrictionModel());
			//double forcex = f*speedx;
			//double forcey = f*speedy;
			double force[] = frictionModel.getFrictionForce(d, speedx, speedy);
			double deltamx = dt * force[0];
			double deltamy = dt * force[1];
			momentumx += deltamx;
			momentumy += deltamy;
			angularMomentum += angularMomentum(d.getX(), d.getY(), deltamx, deltamy, coordinates.getOriginX(), coordinates.getOriginY());
		}
	}

	/**
	 * Is the disk complex translating or rotating
	 * 
	 * @return true if one of the speeds is not 0.0
	 */
	public boolean isMoving() {
		return (getSpeedx() != 0.0) || (getSpeedy() != 0.0) || (getAngularSpeed() != 0.0);
	}

	public double getMaxTimeStep() {
		double maxDisplacement = minDiskRadius * MAX_RELATIVE_DISK_TIMESTEP_DISPLACEMENT;
		double maxRotationAngle = minDiskRadiusOverDistanceToOrigin * MAX_RELATIVE_DISK_TIMESTEP_DISPLACEMENT;
		double maxdtx = maxDisplacement / Math.abs(getSpeedx());
		double maxdty = maxDisplacement / Math.abs(getSpeedy());
		double maxdta = maxRotationAngle / Math.abs(getAngularSpeed());
		double max = Double.MAX_VALUE;
		if (!Double.isNaN(maxdtx))
			max = Math.min(max, maxdtx);
		if (!Double.isNaN(maxdty))
			max = Math.min(max, maxdty);
		if (!Double.isNaN(maxdta))
			max = Math.min(max, maxdta);
		return max;
	}

	/**
	 * An impulse (= force x time) at a given point is applied to the DiskComplex.
	 * 
	 * @param impulseX
	 *            x component of the impulse
	 * @param impulseY
	 *            y component of the impulse
	 * @param pointX
	 *            x coordinate (absolute) of the point at which the impulse is applied
	 * @param pointY
	 *            y coordinate (absolute) of the point at which the impulse is applied
	 */
	public void applyImpulse(double impulseX, double impulseY, double pointX, double pointY) {
		if (isMerged()) {
			Log.abort("cannot apply impulse for merged complexes");
		}
		//		System.out.println("-----------------------------");
		//		System.out.println("impulse "+impulseX+","+impulseY);
		//		System.out.println("point "+pointX+","+pointY);
		//		System.out.println("center "+getCenterx()+","+getCentery());
		momentumx += impulseX;
		momentumy += impulseY;
		angularMomentum += angularMomentum(pointX, pointY, impulseX, impulseY, coordinates.getOriginX(), coordinates.getOriginY());
		reCalculateSpeeds();
		//		System.out.println("Speedx: "+speedx);
		//		System.out.println("AngularSpeed: "+angularSpeed);
		//		System.out.println("-----------------------------");
		//		impulseApplied = true;
	}

	public void applyFrictionImpulses(Map<Disk, double[]> newPositions, double dt, FrictionModel frictionModel) {
		for (Entry<Disk, double[]> e : newPositions.entrySet()) {
			Disk d = e.getKey();
			double[] pos = e.getValue();
			double f = 1.0 / dt;
			double sx = (pos[0] - d.getX()) * f;
			double sy = (pos[1] - d.getY()) * f;
			applyFrictionImpulse(d, sx, sy, dt, frictionModel);
		}
		reCalculateSpeeds();
	}

	/**
	 * Get dot product of speed at a point with the given direction vector
	 * 
	 * @param pointX
	 * @param pointY
	 * @param directionX
	 * @param directionY
	 * @return
	 */
	public double getProjectedSpeed(double pointX, double pointY, double directionX, double directionY) {
		return getSpeedx(pointX, pointY) * directionX + getSpeedy(pointX, pointY) * directionY;
	}

	/**
	 * How much will the value getProjectedSpeed() change as an effect of the applyImpulse() method (where the impulse is equal to the direction vector)
	 * 
	 * @param pointX
	 * @param pointY
	 * @param directionX
	 * @param directionY
	 * @return change
	 */
	public double getProjectedSpeedChangeByImpulse(double pointX, double pointY, double dirx, double diry) {
		// How much does (mx/m - (y-cy)*am/mm)*dx + (my/m + (x-cx)*am/mm)*dy change when
		// mx += dx;
		// my += dy;
		// am += rx*dy-ry*dx;
		// where rx := x-cx; ry := y-cy
		// 
		// Answer: 
		// (dx/m - ry*(rx*dy-ry*dx)/mm)*dx + (dy/m + rx*(rx*dy-ry*dx)/mm)*dy
		// (dx^2 + dy^2)/m + (-ry*(rx*dy-ry*dx)*dx + rx*(rx*dy-ry*dx)*dy)/mm
		// (dx^2 + dy^2)/m + (dy*rx-dx*ry)^2/mm
		double rx = pointX - getCenterx();
		double ry = pointY - getCentery();
		double dyrx_dxry = diry * rx - dirx * ry;
		double dx2_dy2 = dirx * dirx + diry * diry;
		return dx2_dy2 / mass + dyrx_dxry * dyrx_dxry / massMomentum;
	}

	/**
	 * Angular momentum (p-c) x m of a linear momentum m that acts at a point p, respective to centre c
	 * 
	 * @param px
	 *            x coordinate of point at which the momentum acts
	 * @param py
	 *            y coordinate of point at which the momentum acts
	 * @param mx
	 *            x component of linear momentum
	 * @param my
	 *            y component of linear momentum
	 * @param cx
	 *            x coordinate of rotation center
	 * @param cy
	 *            y coordinate of rotation center
	 * @return angular momentum (z component of cross product r x m)
	 */
	public static double angularMomentum(double px, double py, double mx, double my, double cx, double cy) {
		return (px - cx) * my - (py - cy) * mx;
	}

	/**
	 * This method must be called whenever the structure has changed, i.e. when - disks have been added - disks have been removed - disk positions have been changed - disk sizes/masses have changed
	 * 
	 * Calculates the new center of mass and shifts origin of the coordinate system there. Calculates the new mass and massMomentum, uses old momenta values to update speeds
	 * 
	 */
	public void recalculateAfterStructuralChange() {
		recaluclateCenterOfMass();
		recalculateMassMomentum();
		recalculateMinRadius();
		reCalculateSpeeds();
	}

	private void recalculateMinRadius() {
		// calculate new minimal disk radius and minimal quotient radius/distance to origin
		minDiskRadius = Double.POSITIVE_INFINITY; //Limits.MAX_DISK_DIAMETER/2.0;
		minDiskRadiusOverDistanceToOrigin = Double.POSITIVE_INFINITY;
		for (Disk d : disks) {
			minDiskRadius = Math.min(minDiskRadius, d.getRadius());
			double dist = d.getDistanceToOrigin();
			if (dist > 0.0) {
				minDiskRadiusOverDistanceToOrigin = Math.min(minDiskRadiusOverDistanceToOrigin, d.getRadius() / dist);
			}
		}
	}

	private void recalculateMassMomentum() {
		// new mass and massMomentum
		massMomentum = 0.0;
		massRadiusSum = 0.0;
		for (Disk d : disks) {
			double dist = d.getDistanceToOrigin();
			double r = d.getRadius();
			massMomentum += d.getMass() * (0.5 * r * r + dist * dist); // massMomentum of disk plus shift by Steiner
			massRadiusSum += d.getMass() * dist;
		}
	}

	private void recaluclateCenterOfMass() {
		// calculate new center of mass
		double sumx = 0.0;
		double sumy = 0.0;
		double summ = 0.0;
		for (Disk d : disks) {
			double m = d.getMass();
			sumx += m * d.getX();
			sumy += m * d.getY();
			summ += m;
		}
		mass = summ;
		double comx = sumx / summ;
		double comy = sumy / summ;
		// set new coordinate origin
		double deltax = comx - coordinates.getOriginX();
		double deltay = comy - coordinates.getOriginY();
		coordinates.setOrigin(comx, comy);
		// this has moved all disks absolute positions by deltax, deltay; have to shift all back 
		for (Disk d : disks) {
			d.shiftPosition(-deltax, -deltay);
		}
	}

	public void merge(DiskComplex other) {
		if (isMerged()) {
			Log.abort("cannot do nested merge");
		}
		other = other.getRootInMergeTree();
		if (other == this) {
			Log.abort("cannot merge to itself");
		}
		double jointmomentumx = momentumx + other.momentumx;
		double jointmomentumy = momentumy + other.momentumy;
		double jointAngularMomentum = jointAngularMomentum(this, other);
		for (Disk disk : other.disks) {
			disk.changeOwner(this);
			disks.add(disk);
		}
		momentumx = jointmomentumx;
		momentumy = jointmomentumy;
		angularMomentum = jointAngularMomentum;
		recalculateAfterStructuralChange();
		other.mergedTo = this;
		//		impulseApplied = impulseApplied || other.impulseApplied;
		//		other.impulseApplied = false;
		//collisions.putAll(other.collisions);
		//other.collisions.clear();
	}

	private static double jointAngularMomentum(DiskComplex dc1, DiskComplex dc2) {
		// calculate joint center of mass
		double x1 = dc1.getCenterx();
		double y1 = dc1.getCentery();
		double m1 = dc1.getMass();
		double x2 = dc2.getCenterx();
		double y2 = dc2.getCentery();
		double m2 = dc2.getMass();
		double f = 1.0 / (m1 + m2);
		double comx = (x1 * m1 + x2 * m2) * f;
		double comy = (y1 * m1 + y2 * m2) * f;
		// joint angular momentum: sum of individual angular momenta (around individual centers of mass) plus orbit angular momenta (around joint center of mass)
		return dc1.angularMomentum + dc2.angularMomentum + dc1.orbitAngularMomentum(comx, comy) + dc2.orbitAngularMomentum(comx, comy);
	}

	/**
	 * The angular momentum of all masses concentrated in the center of gravity around reference point
	 */
	private double orbitAngularMomentum(double refPointx, double refPointy) {
		return angularMomentum(getCenterx(), getCentery(), momentumx, momentumy, refPointx, refPointy);
	}

	void addNewDisk(Disk newDisk) {
		DiskComplex target = this;
		do {
			target.disks.add(newDisk);
			if (!target.isMerged()) {
				target.recalculateAfterStructuralChange();
			}
			target = target.mergedTo;
		} while (target != null);
	}

	/**
	 * Split off the disk of a DiskComplex which has been merged to this
	 * 
	 * @param merged
	 *            the preciously merged disk complex
	 */
	public void split() {
		if (!isMerged()) {
			Log.abort("is not merged");
		}
		if (mergedTo.isMerged()) {
			Log.abort("super is not merged");
		}
		DiskComplex root = mergedTo;
		reCaluculateMomenta(root);
		// assign old coordinate system, remove disks of merged from this
		for (Disk disk : disks) {
			disk.changeOwner(this);
			root.disks.remove(disk);
		}
		recalculateAfterStructuralChange();
		//		impulseApplied = root.impulseApplied;
		root.reCaluculateMomenta(root);
		root.recalculateAfterStructuralChange();
		mergedTo = null;
	}

	/**
	 * Calulcates the momenta of this DiskComplex, under the assumption that this had been merged to mergedTo
	 */
	private void reCaluculateMomenta(DiskComplex reference) {
		// calculate center of mass of all disks 
		double sumx = 0.0;
		double sumy = 0.0;
		double summ = 0.0;
		for (Disk disk : disks) {
			double m = disk.getMass();
			sumx += disk.getX() * m;
			sumy += disk.getY() * m;
			summ += m;
		}
		double comx = sumx / summ;
		double comy = sumy / summ;

		// calculate momentum of the split off DiskComplex using momenta from reference disk complex 
		double sumMomentumx = 0.0;
		double sumMomentumy = 0.0;
		double sumAngularMomentum = 0.0;
		for (Disk disk : disks) {
			double mx = reference.momentumX(disk);
			double my = reference.momentumY(disk);
			sumMomentumx += mx;
			sumMomentumy += my;
			sumAngularMomentum += angularMomentum(disk.getX(), disk.getY(), mx, my, comx, comy) + reference.spinAngularMomentum(disk);
		}
		momentumx = sumMomentumx;
		momentumy = sumMomentumy;
		angularMomentum = sumAngularMomentum;
	}

	/**
	 * The x component of the momentum of a disk
	 */
	private double momentumX(Disk disk) {
		return getSpeedx(disk) * disk.getMass();
	}

	/**
	 * The x component of the momentum of a disk
	 */
	private double momentumY(Disk disk) {
		return getSpeedy(disk) * disk.getMass();
	}

	/**
	 * The spin angular momentum of a rotating disk
	 */
	private double spinAngularMomentum(Disk disk) {
		double r = disk.getRadius();
		return 0.5 * disk.getMass() * r * r * getAngularSpeed();
	}

	private void reCalculateSpeeds() {
		speedx = momentumx / mass;
		speedy = momentumy / mass;
		angularSpeed = angularMomentum / massMomentum;
		if (Double.isNaN(angularSpeed)) {
			System.out.println("angularMomentum=" + angularMomentum);
			System.out.println("massMomentum=" + massMomentum);
			throw new RuntimeException("NAN occured");
		}
	}

	public DiskComplex getRootInMergeTree() {
		DiskComplex ret = this;
		while (ret.isMerged()) {
			ret = ret.mergedTo;
		}
		return ret;
	}

	public void setStatical(boolean statical) {
		this.statical = statical;
	}

	public boolean isStatical() {
		return this.statical;
	}

}
