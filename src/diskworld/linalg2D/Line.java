package diskworld.linalg2D;

public class Line {
	private final Point p1,p2;
	
	public Line(Point start, Point end) {
		this.p1 = start;
		this.p2 = end;
	}

	public double getX1() {
		return p1.getX();
	}

	public double getY1() {
		return p1.getY();
	}

	public double getX2() {
		return p2.getX();
	}

	public double getY2() {
		return p2.getY();
	}

	public double getDeltaX() {
		return p2.getX()-p1.getX();
	}

	public double getDeltaY() {
		return p2.getY()-p1.getY();
	}

	public double getLength() {
		double dx = getDeltaX();
		double dy = getDeltaY();
		return Math.sqrt(dx*dx+dy*dy);
	}

}
