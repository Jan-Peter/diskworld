package diskworld.linalg2D;

public class Vector {
	public double x;
	public double y;

	public Vector(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public void incMul(double s, Vector v) {
		x += s*v.x;
		y += s*v.y;
	}

	public double length() {
		return Math.sqrt(x*x + y*y);
	}

	public void decLength(double lengthRed) {
		double oldl = length();
		double newl = oldl - lengthRed;
		if (newl <= 0.0) {
			x = 0.0;
			y = 0.0;
		} else {
			double f = newl/oldl;
			x *= f;
			y *= f;
		}
	}
}
