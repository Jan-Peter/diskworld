package diskworld.linalg2D;

public class Point {
	public double x;
	public double y;
	
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public void incMul(double d, Vector v) {
		x += d*v.x;
		y += d*v.y;
	}
	
	public double distanceSqr(double x, double y) {
		double dx = this.x - x;
		double dy = this.y - y;
		return dx*dx+dy*dy;
	}

	public double distance(double x, double y) {
		double dx = this.x - x;
		double dy = this.y - y;
		return Math.sqrt(dx*dx+dy*dy);
	}

	public Vector direction(Point p) {
		double dx = p.x - x;
		double dy = p.y - y;
		double d = Math.sqrt(dx*dx+dy*dy);
		if (d == 0.0)
			return null;
		double f = 1.0/d;
		return new Vector(dx*f,dy*f);
	}

	public void set(double x, double y) {
		this.x = x;
		this.y = y;	
	}

	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}

}
