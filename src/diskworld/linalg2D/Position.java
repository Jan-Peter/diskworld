package diskworld.linalg2D;

public interface Position {
	public double getAbsX();
	public double getAbsY();
	public double getAbsAngle();
}
