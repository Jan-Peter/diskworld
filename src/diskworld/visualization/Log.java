package diskworld.visualization;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Provides logging and abort functionality.
 */
public class Log {

	/**
	 * Enum for specifying the level of detail 
	 */
	public static enum Loglevel {
		SEVERE, WARNING, DEFAULT, DETAILED, DEBUG
	};
	
	private static PrintStream logStream = System.out;
	private static PrintStream errStream = System.err;
	
	// /*use a final loglevel to allow compiler optimizations:*/
	// private static final Loglevel logLevel = Loglevel.DEFAULT;
	
	/*use a changable loglevel:*/
    private static Loglevel logLevel = Loglevel.DEFAULT;
    
	/**
	 * Set the level of detail for logging
	 * 
	 * @param newLogLevel
	 */
	public static void setLogLevel(Loglevel newLogLevel) {
		logLevel = newLogLevel;
	}
	
	/**
	 * Read log level from a string
	 * 
	 * @param logLevelStr the log level as text 
	 */
	public static Loglevel parse(String logLevelStr) {
		String cmp = logLevelStr.trim().toLowerCase();
		if (cmp.equals("severe")) {
			return Loglevel.SEVERE;
		}
		if (cmp.equals("warning")) {
			return Loglevel.WARNING;
		}
		if (cmp.equals("default")) {
			return Loglevel.DEFAULT;
		}
		if (cmp.equals("detailed")) {
			return Loglevel.DETAILED;
		}
		if (cmp.equals("debug")) {
			return Loglevel.DEBUG;
		}
		Log.warning("Unknon log level: "+cmp);
		return Loglevel.DEFAULT;
	}


	/**
	 * Set the PrintStream to which normal (>= LogLevel.DEFAULT) logs are printed.
	 * By default System.out is used.
	 * 
	 * @param newLogStream the new PrintStream to be used
	 */
	public static void setLogStream(PrintStream newLogStream) {
		logStream = newLogStream;
	}

	/**
	 * Set the PrintStream to which abnormal (LogLevel.SEVERE or LogLevel.WARNING) logs are printed.
	 * By default System.err is used.
	 * 
	 * @param newErrStream the new PrintStream to be used
	 */
	public static void setErrorWarningStream(PrintStream newErrStream) {
		errStream = newErrStream;
	}

	/**
	 * Log a message if the provided LogLevel is active.
	 * 
	 * @param message the String to be printed
	 * @param level the Loglevel beyond which this message is supposed to become visible
	 */
	public static void log(String message, Loglevel level) {
		if (level.compareTo(logLevel) <= 0) {
			if (level==Loglevel.SEVERE) {
				synchronized (errStream) {
					errStream.println(message);	
				}
			} else {
				synchronized (logStream) {
					logStream.println(message);	
				}			
			}
		}
	}

	/**
	 * Log a message that indicates a critical failure
	 * 
	 * @param message the String to be printed
	 */
	public static void severe(String message) {
		log(message,Loglevel.SEVERE);
	}
	
	/**
	 * Log a message that warns about a critical inconsistency/possible failure
	 * 
	 * @param message the String to be printed
	 */
	public static void warning(String message) {
		log(message,Loglevel.WARNING);
	}

	/**
	 * Log a message with normal status information (highest level of abstraction)
	 * 
	 * @param message the String to be printed
	 */
	public static void log(String message) {
		log(message,Loglevel.DEFAULT);
	}

	/**
	 * Log a message with normal status information (medium level of abstraction)
	 * 
	 * @param message the String to be printed
	 */
	public static void detail(String message) {
		log(message,Loglevel.DETAILED);
	}

	/**
	 * Log a message with normal status information (lowest level of abstraction)
	 * 
	 * @param message the String to be printed
	 */
	public static void debug(String message) {
		log(message,Loglevel.DEBUG);
	}

	/**
	 * Test if messages of the LogLevel.DETAILED will be displayed.
	 * This can be used to avoid time-consuming preparation of detailed log information when the LogLevel 
	 * is not high enough.
	 * 
	 * @return true if messages with LogLevel.DETAILED will be displayed.
	 */
	public static boolean logDetailed() {
		return (Loglevel.DETAILED.compareTo(logLevel) <= 0); 
	}

	/**
	 * Test if messages of the LogLevel.DEBUG will be displayed.
	 * This can be used to avoid time-consuming preparation of detailed log information when the LogLevel 
	 * is not high enough.
	 * 
	 * @return true if messages with LogLevel.DEBUG will be displayed.
	 */
	public static boolean logDebug() {
		return (Loglevel.DEBUG.compareTo(logLevel) <= 0); 
	}

	/**
	 * Print a warning and stop the java runtime.
	 * 
	 * @param exitcode the error level to be passed to the calling environment
	 */
	public static void abort(int exitcode) {
		warning("Exiting system with error level "+exitcode);
		System.exit(exitcode);
	}

	/**
	 * Print a stack trace and stop the java runtime.
	 * 
	 * @param e error or exception for which the stack trace shall be printed
	 * @param exitcode the error level to be passed to the calling environment
	 */
	public static void abort(Throwable e, int exitcode) {
		log(e);
		abort(exitcode);
	}

	/**
	 * Print a stack trace and stop the java runtime (with exit code 1).
	 * 
	 * @param e error or exception for which the stack trace shall be printed
	 */
	public static void abort(Throwable e) {
		abort(e,1);
	}

	/**
	 * Print a message and stop the java runtime (with exit code 1).
	 * 
	 * @param message the String to be printed before system exit
	 */
	public static void abort(String message) {
		severe(message);
		abort(1);
	}

	/**
	 * Print a stack trace (the system continues running).
	 * 
	 * @param e error or exception for which the stack trace shall be printed
	 */
	private static void log(Throwable e) {
		synchronized (errStream) {
			e.printStackTrace(errStream);
		}
	}

	@SuppressWarnings("rawtypes")
	private static Map<Class,Integer> runnableCounts = new HashMap<Class, Integer>(); 
	
	/**
	 * Create a envelope around a Runnable (used to create Thread) that captures and logs all errors and exceptions
	 * thrown by the run() method. Start and termination of the runnable are logged with LogLevel.DEBUG 
	 *  
	 * @param runnable the Runnable to be protected against uncaught exceptions
	 * @return the enveloped Runnable
	 */
	public static Runnable getLoggedRunnable(final Runnable runnable) {
		int count;
		synchronized (runnableCounts) {
			Integer countObj = runnableCounts.get(runnable.getClass());
			count = countObj == null ? 0 : countObj+1;
			runnableCounts.put(runnable.getClass(), count);
		}
		final int runnableCount = count;
		Runnable lr = new Runnable() {
			@Override
			public void run() {
				debug("Started: runnable #"+runnableCount+" of class "+runnable.getClass().getName());	
				try {
					runnable.run();
				} catch (Throwable e) {
					severe("Exception "+e.toString()+" in runnable #"+runnableCount+" of class "+runnable.getClass().getName());
					log(e);
					if (e instanceof Error) {
						abort(e);
					}
				}
				debug("Terminated: runnable #"+runnableCount+" of class "+runnable.getClass().getName());	
			}
		};
		return lr;
	}
 
}
