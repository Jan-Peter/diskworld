package diskworld.visualization;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import diskworld.diskcomplexes.Disk;
import diskworld.diskcomplexes.DiskComplex;
import diskworld.environment.Environment;
import diskworld.environment.Floor;
import diskworld.environment.Wall;

public class PaintableEnvironmentClone {

	private static final int TIME_POS_X = 10;
	private static final int TIME_POS_Y = 20;

	private final Floor floor;
	private final Collection<Wall> walls;
	private final List<PaintableDisk> disks;
	private final double time;

	public PaintableEnvironmentClone(Environment environment) {
		this.floor = environment.getFloor().createClone();
		this.walls = environment.getWalls(); // no need for cloning, walls do not change
		disks = new LinkedList<PaintableDisk>();
		for (DiskComplex dc : environment.getDiskComplexes()) {
			if (!dc.isMerged()) {
				for (Disk d : dc.getDisks()) {
					disks.add(new PaintableDisk(d));
				}
			}
		}
		this.time = environment.getTime();
	}

	public void paint(Graphics2D g, VisualizationSettings settings) {
		if (settings.isOptionEnabled(VisualizationOptions.OPTION_FLOOR)) {
			paintFloor(g, settings);
		} else {
			clear(g, settings);
		}
		if (settings.isOptionEnabled(VisualizationOptions.OPTION_WALLS)) {
			for (Wall w : walls) {
				paintWall(g, w, settings);
			}
		}
		if (settings.isOptionEnabled(VisualizationOptions.OPTION_DISKS)) {
			int paintSymbols = settings.getOptions().getOption(VisualizationOptions.GROUP_GENERAL, VisualizationOptions.OPTION_DISK_SYMBOLS).chosenVariantIndex();
			for (PaintableDisk d : disks) {
				d.paint(g, settings, paintSymbols);
			}
		}
		if (settings.isOptionEnabled(VisualizationOptions.OPTION_TIME)) {
			paintTime(g, settings);
		}
	}

	private void paintTime(Graphics2D g, VisualizationSettings settings) {
		g.setColor(Color.BLACK);
		g.drawString("t=" + String.format("%3.2f", time), TIME_POS_X, TIME_POS_Y);
	}

	private static void paintWall(Graphics2D g, Wall w, VisualizationSettings settings) {
		g.setColor(settings.getWallColor());
		double x1 = w.getX1();
		double y1 = w.getY1();
		double x2 = w.getX2();
		double y2 = w.getY2();
		double d = w.getHalfThickness();
		double dx = w.getHalfThicknessX();
		double dy = w.getHalfThicknessY();
		PaintableDisk.paintCircle(g, x1, y1, d, settings);
		PaintableDisk.paintCircle(g, x2, y2, d, settings);
		paintRectangle(g, x1 - dx, y1 - dy, x1 + dx, y1 + dy, x2 + dx, y2 + dy, x2 - dx, y2 - dy, settings);
	}

	private static void paintRectangle(Graphics2D g, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, VisualizationSettings settings) {
		g.fillPolygon(new int[] { settings.mapX(x1), settings.mapX(x2), settings.mapX(x3), settings.mapX(x4) },
				new int[] { settings.mapY(y1), settings.mapY(y2), settings.mapY(y3), settings.mapY(y4) }, 4);
	}

	private void paintFloor(Graphics2D g, VisualizationSettings settings) {
		int x1 = settings.mapX(floor.getPosX(0));
		for (int i = 0; i < floor.getNumX(); i++) {
			int y1 = settings.mapY(floor.getPosY(0));
			int x2 = settings.mapX(floor.getPosX(i + 1));
			for (int j = 0; j < floor.getNumY(); j++) {
				int y2 = settings.mapY(floor.getPosY(j + 1));
				g.setColor(settings.getFloorColor(floor.getType(i, j)));
				g.fillRect(x1, y2, x2 - x1, y1 - y2);
				y1 = y2;
			}
			x1 = x2;
		}
	}

	private void clear(Graphics2D g, VisualizationSettings settings) {
		int x1 = settings.mapX(floor.getPosX(0));
		int y1 = settings.mapY(floor.getPosY(0));
		int x2 = settings.mapX(floor.getPosX(floor.getNumX()));
		int y2 = settings.mapY(floor.getPosY(floor.getNumY()));
		g.setColor(settings.getFloorColor(0));
		g.fillRect(x1, y2, x2 - x1, y1 - y2);
	}

}
