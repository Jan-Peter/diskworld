package diskworld.visualization;

import java.awt.Graphics2D;

public class CircleDiskSymbol extends AbstractDiskSymbol {

	private double relRadius;
	
	public CircleDiskSymbol(double relativeRadius) {
		this.relRadius = relativeRadius;
	}
	
	@Override
	public void drawSymbol(Graphics2D graphics, int screenx, int screeny, int halfwidth, int halfheight, double angle) {
		int rx = (int) Math.round(halfwidth*relRadius);
		int ry = (int) Math.round(halfheight*relRadius);		
		graphics.fillArc(screenx-rx, screeny-ry, 2*rx+1, 2*ry+1, 0, 360);
	}

}
