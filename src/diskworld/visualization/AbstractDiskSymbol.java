package diskworld.visualization;

import java.awt.Color;
import java.awt.Graphics2D;

import diskworld.diskcomplexes.DiskType;

public abstract class AbstractDiskSymbol implements VisualisationItem {

	public abstract void drawSymbol(Graphics2D graphics, int screenx, int screeny, int halfwidth, int halfheight, double angle);

	@Override
	public void draw(Graphics2D g, double centerx, double centery, double radius, double angle, double activity, double measurement[], VisualizationSettings settings, DiskType diskType) {
		VisualizationOption option = settings.getOptions().getOption(VisualizationOptions.GROUP_GENERAL, VisualizationOptions.OPTION_DISK_SYMBOLS);
		if (option.isEnabled()) {
			Color col = (option.chosenVariantIndex() == 0) ? settings.getActivityColor(activity) : settings.getDiskSymbolColor(diskType.getColorIndex());
			g.setColor(col);
			int x = settings.mapX(centerx);
			int y = settings.mapY(centery);
			int x1 = settings.mapX(centerx - radius);
			int y1 = settings.mapY(centery + radius);
			drawSymbol(g, x, y, x - x1, y - y1, angle);
		}
	}

}
