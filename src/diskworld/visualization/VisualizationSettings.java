package diskworld.visualization;

import java.awt.Color;
import java.awt.color.ColorSpace;
import java.util.Random;

import diskworld.environment.Environment;

public class VisualizationSettings {
	private static final int NUM_TYPES = 255;

	private Color[] floorColors;
	private Color[] diskColors;
	private Color[] diskSymbolColors;
	private Color activity_0_color;
	private Color activity_plus1_color;
	private Color activity_minus1_color;
	private Color wallColor;
	private Color sensorShapeBorder;
	private Color sensorShapeFill;
	private Color sensorValueTextColor;
	private Color sensorValueGraphicalColor;

	private double minx, miny, maxx, maxy;
	private double factorx, factory;
	private int width, height;

	private VisualizationOptions options;

	public VisualizationSettings() {
		minx = 0.0;
		miny = 0.0;
		maxx = 1.0;
		maxy = 1.0;
		width = 0;
		height = 0;
		factorx = 0;
		factory = 0;
		Random random = new Random(0);
		floorColors = new Color[NUM_TYPES];
		for (int i = 0; i < floorColors.length; i++)
			floorColors[i] = getRandomSaturatedColor(random);
		diskColors = new Color[NUM_TYPES];
		diskSymbolColors = new Color[NUM_TYPES];
		random = new Random(1);
		for (int i = 0; i < diskColors.length; i++) {
			diskColors[i] = getRandomSaturatedColor(random);
			diskSymbolColors[i] = inverted(diskColors[i]);
		}
		activity_0_color = Color.BLACK;
		activity_plus1_color = Color.GREEN;
		activity_minus1_color = Color.RED;
		wallColor = Color.DARK_GRAY;
		sensorShapeBorder = Color.YELLOW;
		sensorShapeFill = new Color(1.0f, 1.0f, 1.0f, 0.2f);
		sensorValueTextColor = Color.BLACK;
		sensorValueGraphicalColor = Color.MAGENTA;
		options = new VisualizationOptions();
	}

	private static Color inverted(Color color) {
		ColorSpace cs = color.getColorSpace();
		float cf[] = color.getColorComponents(null);
		for (int i = 0; i < cf.length; i++) {
			cf[i] = cs.getMinValue(i) + (cs.getMaxValue(i) - cf[i]);
		}
		return new Color(cs, cf, color.getAlpha() / 255.0f);
	}

	public VisualizationOptions getOptions() {
		return options;
	}

	public boolean isOptionEnabled(String group, String key) {
		return options.getOption(group, key).isEnabled();
	}

	public boolean isOptionEnabled(String key) {
		return options.getOption(VisualizationOptions.GROUP_GENERAL, key).isEnabled();
	}

	private Color getRandomSaturatedColor(Random random) {
		float c1 = random.nextFloat();
		float c2 = random.nextFloat();
		switch (random.nextInt(3)) {
		case 0:
			return new Color(1.0f, c1, c2);
		case 1:
			return new Color(c1, 1.0f, c2);
		case 2:
			return new Color(c1, c2, 1.0f);
		}
		return null;
	}

	public int mapX(double x) {
		return (int) Math.round((x - minx) * factorx);
	}

	public int mapY(double y) {
		return (int) Math.round((maxy - y) * factory);
	}

	void setViewDimension(int width, int height) {
		this.width = width;
		this.height = height;
		calculateFactors();
	}

	public void setViewedRect(double minx, double miny, double maxx, double maxy) {
		this.minx = minx;
		this.maxx = maxx;
		this.miny = miny;
		this.maxy = maxy;
		calculateFactors();
	}

	public void setFullView(Environment env) {
		setViewedRect(0, 0, env.getMaxX(), env.getMaxY());
	}

	private void calculateFactors() {
		factorx = width / (maxx - minx);
		factory = height / (maxy - miny);
	}

	public Color getDiskColor(int index) {
		return diskColors[index];
	}

	public Color getFloorColor(int type) {
		return floorColors[type];
	}

	public Color getWallColor() {
		return wallColor;
	}

	public void setDiskColor(int index, Color color) {
		diskColors[index] = color;
		diskSymbolColors[index] = inverted(color);
	}

	public void setDiskColor(int index, Color color, Color symbolColor) {
		diskColors[index] = color;
		diskSymbolColors[index] = symbolColor;
	}

	public void setFloorColor(int type, Color color) {
		floorColors[type] = color;
	}

	public void setWallColor(Color color) {
		wallColor = color;
	}

	public Color getDiskSymbolColor(int index) {
		return diskSymbolColors[index];
	}

	public Color getActivityColor(double activity) {
		if (activity >= 0.0)
			return interpolate(activity_0_color, activity_plus1_color, (float) activity);
		else
			return interpolate(activity_0_color, activity_minus1_color, (float) -activity);
	}

	private Color interpolate(Color c1, Color c2, float f) {
		float c1f[] = c1.getColorComponents(null);
		float c2f[] = c2.getColorComponents(null);
		for (int i = 0; i < c1f.length; i++) {
			c1f[i] = c1f[i] * (1.0f - f) + c2f[i] * f;
		}
		float alpha = c1.getAlpha() * (1.0f - f) + c2.getAlpha() * f;
		return new Color(c1.getColorSpace(), c1f, alpha / 255.0f);
	}

	public void setActivityColors(Color min, Color zero, Color max) {
		activity_0_color = zero;
		activity_plus1_color = max;
		activity_minus1_color = min;
	}

	public Color getSensorShapeBorder() {
		return sensorShapeBorder;
	}

	public Color getSensorShapeFill() {
		return sensorShapeFill;
	}

	public Color getSensorValueTextColor() {
		return sensorValueTextColor;
	}

	public Color getSensorValueGraphicalColor() {
		return sensorValueGraphicalColor;
	}

}
