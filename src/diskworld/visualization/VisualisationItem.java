package diskworld.visualization;

import java.awt.Graphics2D;

import diskworld.diskcomplexes.DiskType;

/**
 * Interface for additional visualization gadgets assigned to disks (e.g. DiskSymbol, Sensor Range, Debugging info, ...)
 * 
 * @author Jan
 * 
 */
public interface VisualisationItem {
	/**
	 * Draws the item, given absolute coordinates of the disk. Note: use {@link VisualizationSettings#settings.mapX(double)} and {@link VisualizationSettings#settings.mapY(double)} to map the absolute
	 * coordinates to screen coordinates
	 * 
	 * @param g
	 *            the Graphics object used to draw
	 * @param centerx
	 *            x coordinate of disk center
	 * @param centery
	 *            y coordinate of disk center
	 * @param radius
	 *            radius of disk
	 * @param angle
	 *            absolute angle
	 * @param activity
	 *            the disks activity (used for actuators disks)
	 * @param measurement
	 *            for sensor visualization items: array of values measured by the corresponding sensor; otherwise: null
	 * @param settings
	 *            the VisualizationSettings object used to control the drawing
	 * @param diskType
	 *            type of the disk to which this item belongs
	 */
	public void draw(Graphics2D g, double centerx, double centery, double radius, double angle, double activity, double measurement[], VisualizationSettings settings, DiskType diskType);
}
