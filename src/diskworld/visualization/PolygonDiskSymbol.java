package diskworld.visualization;

import java.awt.Graphics2D;
import java.awt.Polygon;

public class PolygonDiskSymbol extends AbstractDiskSymbol {

	private double[][] points;

	private static double[][] TRIANGLE_POINTS = new double[][] {
			{ 1.0, 0.0 },
			{ -0.5, 0.5 * Math.sqrt(3.0) },
			{ -0.5, -0.5 * Math.sqrt(3.0) }
	};

	private static double[][] SQUARE_POINTS = new double[][] {
			{ Math.sqrt(0.5), Math.sqrt(0.5) },
			{ -Math.sqrt(0.5), Math.sqrt(0.5) },
			{ -Math.sqrt(0.5), -Math.sqrt(0.5) },
			{ Math.sqrt(0.5), -Math.sqrt(0.5) },
	};

	public PolygonDiskSymbol(double[][] points) {
		this.points = points;
	}

	public static PolygonDiskSymbol getTriangleSymbol(double relativeRadius) {
		return new PolygonDiskSymbol(scaled(TRIANGLE_POINTS, relativeRadius));
	}

	public static PolygonDiskSymbol getSquareSymbol(double relativeRadius) {
		return new PolygonDiskSymbol(scaled(SQUARE_POINTS, relativeRadius));
	}

	private static double[][] scaled(double[][] p, double relativeRadius) {
		double res[][] = new double[p.length][2];
		for (int i = 0; i < p.length; i++) {
			res[i][0] = p[i][0] * relativeRadius;
			res[i][1] = p[i][1] * relativeRadius;
		}
		return res;
	}

	@Override
	public void drawSymbol(Graphics2D graphics, int screenx, int screeny, int halfwidth, int halfheight, double angle) {
		int xp[] = new int[points.length];
		int yp[] = new int[points.length];
		double s = Math.sin(angle);
		double c = Math.cos(angle);
		for (int i = 0; i < points.length; i++) {
			double x = points[i][0] * c - points[i][1] * s;
			double y = points[i][0] * s + points[i][1] * c;
			xp[i] = screenx + (int) Math.round(x * halfwidth);
			yp[i] = screeny - (int) Math.round(y * halfheight); // note: screen y axis goes downwards 
		}
		Polygon p = new Polygon(xp, yp, points.length);
		graphics.fillPolygon(p);
	}

}
