package diskworld.visualization;

import java.awt.Color; 
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.concurrent.atomic.AtomicReference;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;

import diskworld.environment.Environment;

public class EnvironmentPanel extends JPanel {

	private static final int POPUP_BORDER_THICKNESS = 3;
	private static final Color POPUP_BORDER = Color.GRAY;
	private static final Color POPUP_FOREGROUND = Color.BLACK;
	private static final Color POPUP_BACKGROUND = new Color(255, 240, 240);
	private static final long serialVersionUID = 1L;
	protected static final int POPUPINSET = 15;
	private Environment environment;
	private VisualizationSettings settings;
	private Frame owner;
	private AtomicReference<Window> popup;

	public EnvironmentPanel() {
		super(true);
		setEnvironment(null);
		settings = null;
		this.owner = null;
		popup = new AtomicReference<Window>();
	}

	public void setOwner(Frame owner) {
		this.owner = owner;
	}

	public Environment getEnvironment() {
		return environment;
	}

	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}

	public VisualizationSettings getSettings() {
		if (settings == null) {
			setSettings(new VisualizationSettings());
		}
		return settings;
	}

	public void setSettings(final VisualizationSettings settings) {
		this.settings = settings;
		final JPanel thisPanel = this;
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Window wind = popup.get();
				if (wind != null) {
					wind.dispose();
					popup.set(null);
				} else {
					if (e.getButton() == MouseEvent.BUTTON3) {
						JPanel popupPanel = settings.getOptions().createMenuPanel(POPUP_FOREGROUND, POPUP_BACKGROUND, thisPanel);
						//Popup popup = PopupFactory.getSharedInstance().getPopup(thisPanel, popupPanel, e.getX(), e.getY());
						wind = new Window(owner);
						wind.invalidate();
						popup.set(wind);
						Border outsideBorder = BorderFactory.createLineBorder(POPUP_BORDER, POPUP_BORDER_THICKNESS);
						Border insideBorder = BorderFactory.createEmptyBorder(POPUPINSET, POPUPINSET, POPUPINSET, POPUPINSET);
						popupPanel.setBorder(BorderFactory.createCompoundBorder(outsideBorder, insideBorder));
						wind.add(popupPanel);
						//wind.setUndecorated(true);
						Point loc = thisPanel.getLocationOnScreen();
						wind.setLocation(new Point(loc.x + e.getX(), loc.y + e.getY()));
						wind.validate();
						//popup.setPreferredSize(new Dimension(100, 100));
						//wind.setSize(new Dimension(100, 100));
						wind.setSize(popupPanel.getPreferredSize());
						wind.setAlwaysOnTop(true);
						wind.setVisible(true);
					}
				}
			}
		});
	}

	@Override
	public void paint(Graphics graphics) {
		Graphics2D g = (Graphics2D) graphics;
		if (environment != null) {
			getSettings().setViewDimension(getWidth(), getHeight());
			PaintableEnvironmentClone pec = environment.getPaintableClone();
			pec.paint(g, settings);
		}
	}

}
