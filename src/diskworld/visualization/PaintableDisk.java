package diskworld.visualization;

import java.awt.Graphics2D;

import diskworld.diskcomplexes.Disk;
import diskworld.diskcomplexes.DiskType;

public class PaintableDisk {

	private final double x, y, r, angle, activity;
	private final double[][] measurement;
	private final DiskType type;

	public PaintableDisk(Disk d) {
		x = d.getX();
		y = d.getY();
		r = d.getRadius();
		angle = d.getAngle();
		activity = d.getActivity();
		measurement = deepClone(d.getSensorMeasurements());
		type = d.getDiskType();
	}

	private double[][] deepClone(double[][] arr) {
		if (arr == null)
			return null;
		double res[][] = new double[arr.length][];
		for (int i = 0; i < res.length; i++) {
			res[i] = arr[i].clone();
		}
		return res;
	}

	public void paint(Graphics2D g, VisualizationSettings settings, int paintSymbols) {
		g.setColor(settings.getDiskColor(type.getColorIndex()));
		paintCircle(g, x, y, r, settings);
		int count = 0;
		for (VisualisationItem item : type.getAdditionalVisualisationItems()) {
			// sensor visualisation items are first, can assign measurement by count...
			double values[] = (measurement != null) && (count < measurement.length) ? measurement[count] : null;
			item.draw(g, x, y, r, angle, activity, values, settings, type);
			count++;
		}
	}

	public static void paintCircle(Graphics2D g, double cx, double cy, double r, VisualizationSettings settings) {
		int x1 = settings.mapX(cx - r);
		int y1 = settings.mapY(cy + r);
		int x2 = settings.mapX(cx + r);
		int y2 = settings.mapY(cy - r);
		g.fillArc(x1, y1, x2 - x1, y2 - y1, 0, 360);
	}

}
