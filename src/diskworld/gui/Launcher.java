package diskworld.gui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import diskworld.environment.Environment;
import diskworld.visualization.EnvironmentPanel;
import diskworld.interfaces.RobotController;

public class Launcher {

	/**runs an instance of DiskWorld
	 * 
	 * @param title is the title displayed above the simulation
	 * @param numsteps not used in current implementation
	 * @param sleepTime time to wait in the beginning of execution
	 * @param robotController the robot which does the timesteps
	 * @param TIMESTEP time for one step
	 * @param env the environment
	 * @param fullView whether the whole floor is displayed
	 * @return JFrame with the simulation in it
	 */
	public static JFrame run(String title, int numsteps, int sleepTime,
			RobotController robotController, double TIMESTEP, Environment env, boolean fullView) {
		JFrame frame = new JFrame();
		env.setTime(0);
		EnvironmentPanel ep = new EnvironmentPanel();
		ep.setEnvironment(env);
		//ep.getSettings().setCircleSymbol(10, 0.3); // JK: set symbol in DiskTpe, not any more in settings...
		if(fullView)
			ep.getSettings().setFullView(env);
		// ep.getSettings().setViewedRect(0.0,0.0,0.5,0.5);
		JPanel panel = new JPanel();
		frame.add(panel);
		frame.setTitle(title);
		frame.setVisible(true);
		frame.setSize(new Dimension(620, 700));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JLabel label = new JLabel(title);
		label.setFont(new Font(Font.SERIF, Font.BOLD, 36));
		panel.add(label);
		ep.setPreferredSize(new Dimension(600, 600));
		panel.add(ep);
		frame.add(panel);
		frame.validate();
		final char[] key = new char[1];
		frame.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
				key[0] = e.getKeyChar();
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
			}

			@Override
			public void keyPressed(KeyEvent arg0) {
			}
		});
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		int i=0;
		do {
			i++;
			key[0] = 0;
			long ts = System.currentTimeMillis();
			// env.doTimeStep(0.01);
			double dt = TIMESTEP;
			// System.out.println("DiskComplexes timestep: "+(env.getTime()+dt));
			env.doTimeStep(dt);
//			env.setTime(env.getTime() + dt);//done in doTimeStep()
			// System.out.println("Robot timestep: "+env.getTime()+" speed: "+env.getDiskComplexes().get(0).getSpeedx()+" pos: "+env.getDiskComplexes().get(0).getCenterx());
			robotController.doTimeStep();
			ep.repaint();
			long time = System.currentTimeMillis() - ts;
			long sleep = sleepTime - time;
			if (sleep > 0) {
				try {
					Thread.sleep(sleep);
				} catch (InterruptedException e) {
				}
			}
			// }
		} while (key[0] != ' ' && i< numsteps);
		panel.remove(ep);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		return frame;
	}

}
