package diskworld.tests;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.util.LinkedList;

import javax.swing.JFrame;

import diskworld.diskcomplexes.Disk;
import diskworld.diskcomplexes.DiskComplex;
import diskworld.diskcomplexes.DiskMaterial;
import diskworld.diskcomplexes.DiskType;
import diskworld.environment.Environment;
import diskworld.environment.Wall;
import diskworld.linalg2D.Line;
import diskworld.linalg2D.Point;
import diskworld.visualization.CircleDiskSymbol;
import diskworld.visualization.EnvironmentPanel;
import diskworld.visualization.PolygonDiskSymbol;
import diskworld.visualization.VisualisationItem;
import diskworld.visualization.VisualizationSettings;

public class TestVisualisation {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		Wall w = new Wall(new Line(new Point(0.1, 0.1), new Point(0.9, 0.3)), 0.1);
		LinkedList<Wall> walls = new LinkedList<Wall>();
		walls.add(w);
		Environment env = new Environment(10, 10, 0.1, walls);
		for (int i = 0; i < env.getFloor().getNumX(); i++)
			for (int j = 0; j < env.getFloor().getNumY(); j++)
				env.getFloor().setType(i, j, (byte) ((i + j) % 2));
		DiskComplex d1 = env.getDiskComplexesEnsemble().createNewDiskComplex();
		DiskType dt1 = new DiskType(new DiskMaterial(1, 1));
		dt1.addVisualisationItem(PolygonDiskSymbol.getTriangleSymbol(0.5));
		DiskType dt2 = new DiskType(new DiskMaterial(1, 2));
		Disk d = env.getDiskComplexesEnsemble().addNewDisk(d1, 0.5, 0.45, 0.1, 0, dt1);
		DiskComplex d2 = env.getDiskComplexesEnsemble().createNewDiskComplex();
		d = env.getDiskComplexesEnsemble().addNewDisk(d1, 0.2, 0.7, 0.2, 0, dt2);
		d.setActivity(-0.8);
		DiskType dt3 = new DiskType(new DiskMaterial(1, 3));
		dt3.addVisualisationItem(PolygonDiskSymbol.getSquareSymbol(0.75));

		EnvironmentPanel ep = new EnvironmentPanel();

		// my own visualization, drawing an ellipse with radius 80,50 (not scaled or rotated with disk) 
		ep.getSettings().getOptions().addOption("MyOwnDebugOptions", "FixedEllipses");
		dt3.addVisualisationItem(new VisualisationItem() {
			@Override
			public void draw(Graphics2D g, double centerx, double centery, double radius, double angle, double activity, double measurement[], VisualizationSettings settings, DiskType diskType) {
				if (settings.getOptions().getOption("MyOwnDebugOptions", "FixedEllipses").isEnabled()) {
					int x = settings.mapX(centerx);
					int y = settings.mapY(centery);
					int w = 80;
					int h = 50;
					g.setColor(Color.YELLOW);
					g.drawArc(x - w, y - h, w * 2 + 1, h * 2 + 1, 0, 360);
				}
			}
		});

		env.getDiskComplexesEnsemble().addNewDisk(d1, 0.5, 0.6, 0.05, 0, dt3);

		// rotate a little bit
		env.doTimeStep(0.03);
		System.out.println("Angle before rotation: " + d.getAngle());
		d1.applyImpulse(0, 0.1, 0.4, 0.45);
		d1.applyImpulse(0, -0.1, 0.6, 0.45);
		env.doTimeStep(0.2);
		System.out.println("Angle after rotation: " + d.getAngle());

		ep.setEnvironment(env);
		dt2.addVisualisationItem(new CircleDiskSymbol(0.3));
		ep.getSettings().setViewedRect(0, 0, 1, 1);
		frame.add(ep);
		frame.setVisible(true);
		frame.setSize(new Dimension(600, 600));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
