package diskworld.tests;

import java.util.LinkedList;
import java.util.Random;

import diskworld.diskcomplexes.DiskComplex;
import diskworld.diskcomplexes.DiskComplexEnsemble;
import diskworld.diskcomplexes.DiskMaterial;
import diskworld.diskcomplexes.DiskType;
import diskworld.diskcomplexes.FullSearchCollisionDetector;
import diskworld.environment.Floor;
import diskworld.environment.Wall;

public class TestProjectedSpeedChangeByImpulse {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Floor floor = new Floor(10,10,0.1);
		DiskComplexEnsemble dce = new DiskComplexEnsemble(new FullSearchCollisionDetector(new LinkedList<Wall>()), floor);
		DiskComplex dc = dce.createNewDiskComplex();
		DiskType dt = new DiskType(new DiskMaterial(1.0, 1));
		dce.addNewDisk(dc, 0.5, 0.5, 0.5, 10, dt);
		dce.addNewDisk(dc, 0.7, 0.5, 2.0, 160, dt);
		Random r = new Random();
		for (int i = 0; i < 10; i++) {
			double pointx = r.nextDouble();
			double pointy = r.nextDouble();
			double dx = r.nextDouble();
			double dy = r.nextDouble();
			
			double before = dc.getProjectedSpeed(pointx, pointy, dx, dy);
			double change = dc.getProjectedSpeedChangeByImpulse(pointx, pointy, dx, dy);
			dc.applyImpulse(dx, dy, pointx, pointy);
			double after = dc.getProjectedSpeed(pointx, pointy, dx, dy);
			System.out.println("change "+(after-before)+" predicted: "+change+" okay: "+(Math.abs(after-before-change) < 1e-7));
		}
	}

}
