package diskworld.tests;

import diskworld.linalg2D.CoordinateSystem;
import diskworld.linalg2D.RelativePosition;

public class TestRelPosition {

	public static void main(String args[]) {
		CoordinateSystem coord = new CoordinateSystem();
		coord.setAngle(2.44);
		coord.setOrigin(1.4, 5.3);
		RelativePosition rp = new RelativePosition(coord);
		double absx = 1.24;
		double absy = 0.24;
		double relx = coord.abs2relX(absx, absy);
		double rely = coord.abs2relY(absx, absy);
		System.out.print(coord.rel2absX(relx, rely)-absx);
		System.out.print(",");
		System.out.println(coord.rel2absY(relx, rely)-absy);
		rp.setAbsPosition(absx, absy);
		System.out.print(rp.getAbsX()-absx);
		System.out.print(",");
		System.out.println(rp.getAbsY()-absy);
	}
}
