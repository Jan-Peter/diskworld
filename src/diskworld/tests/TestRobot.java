package diskworld.tests;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.swing.JFrame;

import diskworld.diskcomplexes.Disk;
import diskworld.diskcomplexes.DiskComplex;
import diskworld.diskcomplexes.DiskComplexEnsemble;
import diskworld.diskcomplexes.DiskMaterial;
import diskworld.diskcomplexes.DiskType;
import diskworld.environment.Environment;
import diskworld.environment.Floor;
import diskworld.environment.GlobalFrictionModel;
import diskworld.environment.LocalFrictionModel;
import diskworld.environment.Wall;
import diskworld.gui.Launcher;
import diskworld.interfaces.RobotController;
import diskworld.linalg2D.Line;
import diskworld.linalg2D.Point;
import diskworld.objectTree.MotorCommand;
import diskworld.objectTree.MotorCommand.FixedEnd;
import diskworld.objectTree.ObjectTree;
import diskworld.objectTree.ObjectTreeNode;

public class TestRobot {

	private static final double TIMESTEP = 0.01;
	private static final boolean FULLVIEW = true;
	static JFrame frame = new JFrame();
	static Environment env;

	public static void main(String[] args) {
		env = new Environment(10, 10, 0.1, new LinkedList<Wall>());
		setUniformFloor();
		setElasticity(1.0);
		setFriction(0.0);
		RobotController rc;
		rc = simpleArm();
		frame = Launcher.run("Robot arm (joint limits & self collis.)", 600, 10, rc, TIMESTEP, env, FULLVIEW);

		env = new Environment(10, 10, 0.1, new LinkedList<Wall>());
		rc = octopusArm();
		frame = Launcher.run("Octopus arm", 1000, 10, rc, TIMESTEP, env, FULLVIEW);

		env = new Environment(10, 10, 0.1, new LinkedList<Wall>());
		setFriction(0.1);
		rc = simpleArmFixedSpec(FixedEnd.FRICTION_DEPENDENT);
		frame = Launcher.run("Friction determines which part moves", 600, 10, rc, TIMESTEP, env, FULLVIEW);

		env = new Environment(10, 10, 0.1, new LinkedList<Wall>());
		setFriction(0.1);
		rc = simpleArmFixedSpec(FixedEnd.ROOT);
		frame = Launcher.run("Root is fixed", 600, 10, rc, TIMESTEP, env, FULLVIEW);

		env = new Environment(10, 10, 0.1, new LinkedList<Wall>());
		setFriction(0.1);
		rc = simpleArmFixedSpec(FixedEnd.LEAVES);
		frame = Launcher.run("Leaves are fixed", 600, 10, rc, TIMESTEP, env, FULLVIEW);

		env = new Environment(10, 10, 0.1, new LinkedList<Wall>());
		setFriction(0.1);
		rc = differentMotorCommands();
		frame = Launcher.run("Same robot, different commands", 600, 10, rc, TIMESTEP, env, FULLVIEW);

		env = new Environment(10, 10, 0.1, new LinkedList<Wall>());
		setElasticity(1.0);
		rc = kickArm(false);
		frame = Launcher.run("Kicking objects", 1200, 8, rc, TIMESTEP, env, FULLVIEW);

		env = new Environment(10, 10, 0.1, new LinkedList<Wall>());
		setElasticity(1.0);
		rc = kickArm(true);
		frame = Launcher.run("Fixed root and static DiskComplex", 1200, 8, rc, TIMESTEP, env, FULLVIEW);

		LinkedList<Wall> walls = new LinkedList<Wall>();
		walls.add(new Wall(new Line(new Point(0.2, 0.9), new Point(0.8, 0.9)), 0.05));
		env = new Environment(10, 10, 0.1, walls);
		setElasticity(1.0);
		rc = twoStaticalObjectsColliding();
		frame = Launcher.run("Static robot kicking static wall", 1200, 8, rc, TIMESTEP, env, FULLVIEW);

		env = new Environment(10, 10, 0.1, new LinkedList<Wall>());
		setCheckerBoardFloor();
		setLocationDependentFriction(env.getFloor());
		rc = snake();
		frame = Launcher.run("Snake movement", 1000, 10, rc, TIMESTEP, env, FULLVIEW);

		//	   	setUniformFloor();
		//		setActivityDependentFriction(0.0);
		//	    rc = insect();
		//	   	test("Hexapod",1000,10,rc);

		setElasticity(1.0);
		/*		rc = gripArm();
				test("Grabbing objects",1200,8,rc);*/

		frame.dispose();
		System.exit(0);
	}

	//	public static void test(String title, int numsteps, int sleepTime, RobotController robotController) {
	//		env.setTime(0);
	//		EnvironmentPanel ep = new EnvironmentPanel();
	//		ep.setEnvironment(env);
	//		ep.getSettings().setCircleSymbol(10, 0.3);
	//		//ep.getSettings().setViewedRect(0.0,0.0,0.5,0.5);
	//		JPanel panel = new JPanel();
	//		frame.add(panel);
	//		frame.setTitle(title);
	//		frame.setVisible(true);
	//		frame.setSize(new Dimension(620,700));
	//		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	//		JLabel label = new JLabel(title);
	//		label.setFont(new Font(Font.SERIF,Font.BOLD,36));
	//		panel.add(label);
	//		ep.setPreferredSize(new Dimension(600,600));
	//		panel.add(ep);
	//		frame.add(panel);
	//		frame.validate();
	//		final char[] key = new char[1];
	//		frame.addKeyListener(new KeyListener() {
	//			@Override
	//			public void keyTyped(KeyEvent e) {
	//				key[0] = e.getKeyChar();
	//			}
	//			@Override
	//			public void keyReleased(KeyEvent arg0) {
	//			}
	//			@Override
	//			public void keyPressed(KeyEvent arg0) {
	//			}
	//		});
	//		try {
	//			Thread.sleep(1000);
	//		} catch (InterruptedException e) {
	//		}
	//		do {
	//			key[0] = 0;
	//		    //for (int i = 0; i < numsteps; i++) {
	//			long ts = System.currentTimeMillis();
	//			//env.doTimeStep(0.01);
	//			double dt = TIMESTEP;
	//			//System.out.println("DiskComplexes timestep: "+(env.getTime()+dt));
	//			env.doTimeStep(dt);
	//			env.setTime(env.getTime()+dt);
	//			//System.out.println("Robot timestep: "+env.getTime()+" speed: "+env.getDiskComplexes().get(0).getSpeedx()+" pos: "+env.getDiskComplexes().get(0).getCenterx());
	//			robotController.doTimeStep();
	//			ep.repaint();
	//			long time = System.currentTimeMillis() - ts;
	//			long sleep = sleepTime - time;
	//			if (sleep > 0) {
	//				try {
	//					Thread.sleep(sleep);
	//				} catch (InterruptedException e) {
	//				}
	//			}
	//			//		}
	//		} while (key[0] != ' ');
	//		panel.remove(ep);
	//		try {
	//			Thread.sleep(1000);
	//		} catch (InterruptedException e) {
	//		}
	//	}

	public static void setUniformFloor() {
		for (int i = 0; i < env.getFloor().getNumX(); i++)
			for (int j = 0; j < env.getFloor().getNumY(); j++)
				env.getFloor().setType(i, j, 0);
	}

	public static double massToDensity(double mass, double radius) {
		return mass / (Math.PI * radius * radius);
	}

	public static ObjectTree createTree(double rootRadius, double rootx, double rooty, double rootmass, int color) {
		return ObjectTree.createNewObjectTree(env, rootRadius, rootx, rooty, new DiskType(new DiskMaterial(massToDensity(rootmass, rootRadius), color)));
	}

	public static ObjectTreeNode createChild(ObjectTreeNode node, double angle, double radius, double mass, int color) {
		return node.createChild(angle, radius, new DiskType(new DiskMaterial(massToDensity(mass, radius), color)));
	}

	public static RobotController simpleArm() {
		double size = 0.01;
		final ObjectTree robot = createTree(size, 0.5, 0.5, 1.0, 7);
		final ObjectTreeNode root = robot.getRoot();
		ObjectTreeNode node = root;
		final ObjectTreeNode joint0 = createChild(node, 0, size * 1.5, 0.1, 6);
		node = joint0;
		//joint0.setRotationAngle(-1.5);
		for (int i = 0; i < 5; i++) {
			node = createChild(node, 0, size, 0.1, 7);
		}
		final ObjectTreeNode joint1 = createChild(node, 0, size * 1.5, 0.1, 6);
		node = joint1;
		for (int i = 0; i < 4; i++) {
			node = createChild(node, 0, size, 0.1, 7);
		}
		final ObjectTreeNode joint2 = createChild(node, 0, size * 1.5, 0.1, 6);
		node = joint2;
		for (int i = 0; i < 3; i++) {
			node = createChild(node, 0, size, 0.1, 7);
		}
		createChild(node, 0, size * 2, 0.1, 7);

		return new RobotController() {
			@Override
			public void doTimeStep() {
				List<MotorCommand> plan = new LinkedList<MotorCommand>();
				plan.add(MotorCommand.createAngleChange(joint0, 0.002));
				plan.add(MotorCommand.createAngleChange(joint1, -0.005));
				plan.add(MotorCommand.createAngleChange(joint2, -0.009));
				robot.executeMotorCommands(plan, TIMESTEP, env.getPhysicsParameters());
			}
		};
	}

	public static RobotController octopusArm() {
		double size = 0.05;
		final ObjectTree robot = createTree(size * 2, 0.15, 0.5, 1.0, 7);
		final ObjectTreeNode nodes[] = new ObjectTreeNode[10];
		nodes[0] = createChild(robot.getRoot(), 0, size, 0.1, 6);
		for (int i = 1; i < 10; i++) {
			size *= 0.9;
			nodes[i] = createChild(nodes[i - 1], 0, size, 0.1, 6);
		}
		return new RobotController() {
			@Override
			public void doTimeStep() {
				List<MotorCommand> plan = new LinkedList<MotorCommand>();
				double t = env.getTime();
				for (int i = 0; i < 10; i++) {
					plan.add(MotorCommand.createAngleChange(nodes[i], 0.0001 * (20 + i * i) * Math.cos(0.2 * t * (5 + i / 2))));
				}
				robot.executeMotorCommands(plan, TIMESTEP, env.getPhysicsParameters());
			}
		};
	}

	private static Disk createDisk(DiskComplexEnsemble dce, DiskComplex dc, double x, double y, double radius, double mass, int color) {
		DiskType diskType = new DiskType(new DiskMaterial(mass / (Math.PI * radius * radius), color));
		return dce.addNewDisk(dc, x, y, radius, 0, diskType);
	}

	public static RobotController kickArm(final boolean fixed) {
		double size = 0.01;
		final ObjectTree robot = createTree(size, 0.5, 0.5, 1.0, 6);

		robot.getDiskComplex().setStatical(fixed);

		final ObjectTreeNode root = robot.getRoot();
		final ObjectTreeNode joint0 = createChild(root, 0, size * 2, 0.1, 6);
		ObjectTreeNode node = joint0;
		for (int i = 0; i < 5; i++) {
			node = createChild(node, 0, size, 0.1, 7);
		}
		final ObjectTreeNode joint1 = createChild(node, 0, size * 2, 0.1, 6);
		node = joint1;
		for (int i = 0; i < 5; i++) {
			node = createChild(node, 0, size, 0.1, 7);
		}
		final ObjectTreeNode joint2 = createChild(node, 0, size * 2, 0.1, 6);
		node = joint2;
		for (int i = 0; i < 6; i++) {
			node = createChild(node, 0, size, 0.1, 7);
		}
		List<MotorCommand> plan = new LinkedList<MotorCommand>();
		plan.add(MotorCommand.createAngleChange(joint0, 1.4));
		plan.add(MotorCommand.createAngleChange(joint1, -1.2));
		plan.add(MotorCommand.createAngleChange(joint2, -1.8));
		robot.executeMotorCommands(plan, TIMESTEP, env.getPhysicsParameters());

		//robot.tryChangeAngle(root,+1.8,TIMESTEP);
		//robot.tryChangeAngle(joint1,2.2,TIMESTEP);
		//robot.getDiskComplex().applyImpulse(0.1, 0, 0.5, 0.75);

		DiskComplexEnsemble dce = env.getDiskComplexesEnsemble();
		DiskComplex dc1 = dce.createNewDiskComplex();
		createDisk(dce, dc1, 0.5, 0.9, 0.02, 0.1, 8);
		return new RobotController() {
			@Override
			public void doTimeStep() {
				List<MotorCommand> plan = new LinkedList<MotorCommand>();
				//plan.add(MotorCommand.createAngleChange(joint0,0.002));
				MotorCommand mc1 = MotorCommand.createAngleChange(joint1, 0.002);
				MotorCommand mc2 = MotorCommand.createAngleChange(joint2, 0.003);
				if (fixed) {
					mc1.setFixedEnd(FixedEnd.ROOT);
					mc2.setFixedEnd(FixedEnd.ROOT);
				}
				plan.add(mc1);
				plan.add(mc2);
				robot.executeMotorCommands(plan, TIMESTEP, env.getPhysicsParameters());
			}
		};
	}

	//When a statical diskComplex collides with a wall or another statical diskComplex 
	// nothing should move. Also they should not get stuck in each other.
	public static RobotController twoStaticalObjectsColliding() {
		double size = 0.01;
		final ObjectTree robot = createTree(size, 0.5, 0.5, 1.0, 6);

		robot.getDiskComplex().setStatical(true);

		final ObjectTreeNode root = robot.getRoot();
		final ObjectTreeNode joint0 = createChild(root, 0, size * 2, 0.1, 6);
		ObjectTreeNode node = joint0;
		for (int i = 0; i < 5; i++) {
			node = createChild(node, 0, size, 0.1, 7);
		}
		final ObjectTreeNode joint1 = createChild(node, 0, size * 2, 0.1, 6);
		node = joint1;
		for (int i = 0; i < 5; i++) {
			node = createChild(node, 0, size, 0.1, 7);
		}
		final ObjectTreeNode joint2 = createChild(node, 0, size * 2, 0.1, 6);
		node = joint2;
		for (int i = 0; i < 6; i++) {
			node = createChild(node, 0, size, 0.1, 7);
		}
		List<MotorCommand> plan = new LinkedList<MotorCommand>();
		plan.add(MotorCommand.createAngleChange(joint0, 1.4));
		plan.add(MotorCommand.createAngleChange(joint1, -1.2));
		plan.add(MotorCommand.createAngleChange(joint2, -1.8));
		robot.executeMotorCommands(plan, TIMESTEP, env.getPhysicsParameters());

		return new RobotController() {
			@Override
			public void doTimeStep() {
				List<MotorCommand> plan = new LinkedList<MotorCommand>();
				MotorCommand mc1 = MotorCommand.createAngleChange(joint1, 0.002);
				MotorCommand mc2 = MotorCommand.createAngleChange(joint2, 0.003);
				MotorCommand mc3 = MotorCommand.createAngleChange(joint1, -0.002);
				mc1.setFixedEnd(FixedEnd.ROOT);
				mc2.setFixedEnd(FixedEnd.ROOT);
				mc3.setFixedEnd(FixedEnd.ROOT);
				if (env.getTime() < 13.0) {
					plan.add(mc1);
					plan.add(mc2);
				} else {
					//Just to show that robot is not stuck in wall:
					plan.add(mc3);
				}

				robot.executeMotorCommands(plan, TIMESTEP, env.getPhysicsParameters());
			}
		};
	}

	public static RobotController grabArm() {
		double size = 0.01;
		final ObjectTree robot = createTree(size, 0.5, 0.5, 1.0, 6);
		final ObjectTreeNode root = robot.getRoot();
		final ObjectTreeNode joint0 = createChild(root, 0, size * 2, 0.1, 6);
		ObjectTreeNode node = joint0;
		for (int i = 0; i < 5; i++) {
			node = createChild(node, 0, size, 0.1, 7);
		}
		final ObjectTreeNode joint1 = createChild(node, 0, size * 2, 0.1, 6);
		node = joint1;
		for (int i = 0; i < 5; i++) {
			node = createChild(node, 0, size, 0.1, 7);
		}
		final ObjectTreeNode joint2 = createChild(node, 0, size * 2, 0.1, 6);
		node = joint2;
		for (int i = 0; i < 6; i++) {
			node = createChild(node, 0, size, 0.1, 7);
		}
		List<MotorCommand> plan = new LinkedList<MotorCommand>();
		plan.add(MotorCommand.createAngleChange(joint0, 1.4));
		plan.add(MotorCommand.createAngleChange(joint1, -1.2));
		plan.add(MotorCommand.createAngleChange(joint2, -1.8));
		robot.executeMotorCommands(plan, TIMESTEP, env.getPhysicsParameters());

		//robot.tryChangeAngle(root,+1.8,TIMESTEP);
		//robot.tryChangeAngle(joint1,2.2,TIMESTEP);
		//robot.getDiskComplex().applyImpulse(0.1, 0, 0.5, 0.75);

		DiskComplexEnsemble dce = env.getDiskComplexesEnsemble();
		DiskComplex dc1 = dce.createNewDiskComplex();
		createDisk(dce, dc1, 0.5, 0.9, 0.02, 0.1, 8);
		return new RobotController() {
			@Override
			public void doTimeStep() {
				List<MotorCommand> plan = new LinkedList<MotorCommand>();
				//plan.add(MotorCommand.createAngleChange(joint0,0.002));
				plan.add(MotorCommand.createAngleChange(joint1, 0.002));
				plan.add(MotorCommand.createAngleChange(joint2, 0.003));
				robot.executeMotorCommands(plan, TIMESTEP, env.getPhysicsParameters());
			}
		};
	}

	public static RobotController simpleArmFixedSpec(final FixedEnd fixedEnd) {
		double size = 0.01;
		final ObjectTree robot = createTree(size * 2, 0.3, 0.5, 0.1, 0);
		final ObjectTreeNode root = robot.getRoot();
		ObjectTreeNode node = root;
		for (int i = 0; i < 5; i++) {
			node = createChild(node, 0, size, 0.1, 7);
		}
		final ObjectTreeNode joint0 = createChild(node, 0, size * 1.5, 0.1, 6);
		node = joint0;
		//joint0.setRotationAngle(-1.5);
		for (int i = 0; i < 5; i++) {
			node = createChild(node, 0, size, 0.1, 7);
		}
		final ObjectTreeNode joint1 = createChild(node, 0, size * 1.5, 0.1, 6);
		node = joint1;
		for (int i = 0; i < 4; i++) {
			node = createChild(node, 0, size, 0.1, 7);
		}
		createChild(node, 0, size * 2, 0.1, 7);

		return new RobotController() {
			@Override
			public void doTimeStep() {
				List<MotorCommand> plan = new LinkedList<MotorCommand>();
				MotorCommand mc;
				if (env.getTime() < 3) {
					mc = MotorCommand.createAngleChange(joint0, 0.005);
				} else {
					mc = MotorCommand.createAngleChange(joint1, -0.005);
				}
				mc.setFixedEnd(fixedEnd);
				plan.add(mc);
				robot.executeMotorCommands(plan, TIMESTEP, env.getPhysicsParameters());
			}
		};
	}

	public static RobotController snake() {
		double size = 0.007;
		final ObjectTree robot = createTree(size, 0.4, 0.55, 0.1, 6);
		final ObjectTreeNode nodes[] = new ObjectTreeNode[17];
		nodes[0] = createChild(robot.getRoot(), 0, size, 0.1, 6);
		for (int i = 1; i < nodes.length; i++) {
			nodes[i] = createChild(nodes[i - 1], 0, size, 0.1, 6);
		}
		final double phase[] = new double[nodes.length];
		final double freq[] = new double[nodes.length];
		Random rand = new Random(10);
		final double freq0 = 8;
		final double varPhase = 0.3;
		final double varFreq = 0.1;
		phase[0] = 0;
		freq[0] = freq0;
		for (int i = 1; i < nodes.length; i++) {
			phase[i] = phase[i - 1] + varPhase * 2 * Math.PI * rand.nextDouble();
			freq[i] = freq[i - 1] * (1 + varFreq * rand.nextGaussian());
		}
		return new RobotController() {
			@Override
			public void doTimeStep() {
				List<MotorCommand> plan = new LinkedList<MotorCommand>();
				double t = env.getTime();
				for (int i = 0; i < nodes.length; i++) {
					//plan.add(MotorCommand.createAngleChange(nodes[i],0.0001*(20+i*i)*Math.cos(0.2*t*(5+i/2))));
					plan.add(MotorCommand.createAngleChange(nodes[i], 0.001 * (10) * Math.cos(0.2 * t * (5 + i / 2))));
					//plan.add(MotorCommand.createAngleChange(nodes[i],0.001*ampl*Math.cos(freq[i]*t+phase[i])));
				}
				robot.executeMotorCommands(plan, TIMESTEP, env.getPhysicsParameters());
			}
		};
	}

	/*
	 * The same robot twice. One receives a command to rotate the root, the other one the end
	 * Works when friction = 0. 
	 * TODO BUG: If there exists friction then the robot rotates in both directions: the direction it would
	 * naturally and the direction stated in the command => robot starts spinning around
	 * => Somewhere except ObjectTree.determineMovingParts the moving parts are calculated
	 */
	public static RobotController differentMotorCommands() {
		double size = 0.02;
		final DiskType rootType = new DiskType(new DiskMaterial(massToDensity(0.1, size), 7));
		final DiskType jointType = new DiskType(new DiskMaterial(massToDensity(0.01, size), 6));
		final DiskType endType = new DiskType(new DiskMaterial(massToDensity(0.001, size), 8));
		final ObjectTree robot0 = ObjectTree.createNewObjectTree(env, size, 0.45, 0.7, rootType);
		final ObjectTree robot1 = ObjectTree.createNewObjectTree(env, size, 0.45, 0.4, rootType);

		final ObjectTreeNode joint0 = robot0.getRoot().createChild(0, size, jointType);
		final ObjectTreeNode joint1 = robot1.getRoot().createChild(0, size, jointType);

		@SuppressWarnings("unused")
		final ObjectTreeNode end0 = joint0.createChild(0, size, endType);
		@SuppressWarnings("unused")
		final ObjectTreeNode end1 = joint1.createChild(0, size, endType);

		return new RobotController() {
			@Override
			public void doTimeStep() {

				List<MotorCommand> plan0 = new LinkedList<MotorCommand>();
				plan0.add(MotorCommand.createAngleChange(joint0, 0.005, true));

				List<MotorCommand> plan1 = new LinkedList<MotorCommand>();
				plan1.add(MotorCommand.createAngleChange(joint1, 0.005, false));

				robot0.executeMotorCommands(plan0, TIMESTEP, env.getPhysicsParameters());
				robot1.executeMotorCommands(plan1, TIMESTEP, env.getPhysicsParameters());
			}
		};
	}

	/*private static ObjectTree nPod(int n, int legLength, double sizebody, double sizeleg, ObjectTreeNode jointFoot[][]) {
		double massleg = 0.1;
		double massbody = 0.1;
		final ObjectTree robot = createTree(sizebody/2, 0.5, 0.5, massbody , 6);
		ObjectTreeNode body[] = new  ObjectTreeNode[n];
		body[0] = createChild(robot.getRoot(), 0, sizebody, massbody, 6);
		for (int i = 1; i < n; i++) {
			body[i] = createChild(body[i-1], 0, sizebody, massbody, 6);
		}
		ObjectTreeNode joint[] = new ObjectTreeNode[2*n];
		ObjectTreeNode foot[] = new ObjectTreeNode[2*n];
		for (int i = 0; i < n; i++) {
			joint[2*i] = createChild(body[i], Math.PI/2, sizeleg, massleg, 8);
			foot[2*i] = makeLeg(legLength, sizeleg, joint[2*i],massleg);
			joint[2*i+1] = createChild(body[i], -Math.PI/2, sizeleg, massleg, 8);
			foot[2*i+1] = makeLeg(legLength, sizeleg, joint[2*i+1],massleg);
		}
		jointFoot[0] = joint;
		jointFoot[1] = foot;
		return robot;
	}
	
	private static ObjectTreeNode makeLeg(int legLength, double sizeleg, ObjectTreeNode node, double massleg) {
		for (int j = 0; j < legLength; j++) {
			node = createChild(node, 0, sizeleg, massleg, 7);
		}
		node = createChild(node, 0, sizeleg, massleg*10, 10);
		return node;
	}

	public static RobotController insect()  {
		ObjectTreeNode[][] jointFoot = new ObjectTreeNode[2][];
		final ObjectTree robot = nPod(3, 4, 0.05, 0.008,jointFoot);
		final ObjectTreeNode[] joints = jointFoot[0];
		final ObjectTreeNode[] feet = jointFoot[1];
		final double speed = 0.0002;
		final int group[] = new int[] { 0,1,1,0,0,1 };
		return new RobotController() {
			@Override
			public void doTimeStep() {
				List<MotorCommand> plan = new LinkedList<MotorCommand>();
				double t = env.getTime()*0.2;
				for (int i = 0; i < joints.length; i++) {
					double sign = group[i]*2-1;
					if (t - Math.floor(t) < 0.5) 
						sign *= -1;
					double act = sign*2+1;
					plan.add(MotorCommand.createAngleChange(joints[i],sign*speed));
					//feet[i].setActivity(act);
				}					
				robot.executeMotorCommands(plan, TIMESTEP);
			}
		};
	}*/

	private static void setElasticity(double elasticity) {
		env.getPhysicsParameters().setDisk2DiskElasticty(elasticity);
		env.getPhysicsParameters().setDisk2WallElasticty(elasticity);
	}

	private static void setFriction(final double frictionConstant) {
		env.getPhysicsParameters().setFrictionModel(new GlobalFrictionModel(frictionConstant));
	}

	public static void setCheckerBoardFloor() {
		for (int i = 0; i < env.getFloor().getNumX(); i++)
			for (int j = 0; j < env.getFloor().getNumY(); j++)
				env.getFloor().setType(i, j, 3 * ((i + j) % 2));
	}

	private static void setLocationDependentFriction(final Floor floor) {
		env.getPhysicsParameters().setFrictionModel(new LocalFrictionModel() {
			@Override
			protected double getFloorContactCoefficient(Disk d) {
				int floorType = floor.getType(d.getX(), d.getY());
				return floorType == 0 ? 0.001 : 0.05;
			}
		});
	}

	/*private static void setActivityDependentFriction(final double factor) {
		env.getPhysicsParameters().setFrictionModel(new FrictionModel() {
			@Override
			public double getGlobalFrictionCoefficient() {
				return 0.0;
			}
			@Override
			public double getFrictionCoefficient(Disk d) {
				return (d.getActivity()*9+1)*factor;
			}
			@Override
			public boolean frictionIsGlobal() {
				return false;
			}
		});
	}*/

}
