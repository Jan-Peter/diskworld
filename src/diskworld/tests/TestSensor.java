package diskworld.tests;

import java.awt.Dimension; 
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import diskworld.diskcomplexes.Disk;
import diskworld.diskcomplexes.DiskComplex;
import diskworld.diskcomplexes.DiskComplexEnsemble;
import diskworld.diskcomplexes.DiskMaterial;
import diskworld.diskcomplexes.DiskType;
import diskworld.environment.Environment;
import diskworld.environment.GlobalFrictionModel;
import diskworld.environment.Wall;
import diskworld.interfaces.Sensor;
import diskworld.linalg2D.Line;
import diskworld.linalg2D.Point;
import diskworld.sensors.ColorSensitiveDirectionSensor;
import diskworld.sensors.DiskDistanceSensor;
import diskworld.visualization.EnvironmentPanel;

public class TestSensor {

	static JFrame frame = new JFrame();
	static Environment env;
	static List<DiskComplex[]> mergeList = new LinkedList<DiskComplex[]>();

	public static void main(String[] args) {
		env = new Environment(10, 10, 0.1, getWalls(false));
		setUniformFloor();
		setElasticity(1.0);
		setFriction(0.0);
		sensor1();
		test("Sensor1: Disk Distance (Cone)", 600, 5);
		frame.dispose();
		System.exit(0);
	}

	public static LinkedList<Wall> getWalls(boolean insidewall) {
		LinkedList<Wall> walls = new LinkedList<Wall>();
		walls.add(new Wall(new Line(new Point(0, 0), new Point(1, 0)), 0.01)); // these 4 walls are the outside walls of the frame
		walls.add(new Wall(new Line(new Point(1, 0), new Point(1, 1)), 0.01));
		walls.add(new Wall(new Line(new Point(1, 1), new Point(0, 1)), 0.01));
		walls.add(new Wall(new Line(new Point(0, 1), new Point(0, 0)), 0.01));
		if (insidewall)
			walls.add(new Wall(new Line(new Point(0.3, 0.6), new Point(0.6, 0.2)), 0.05)); // actual wall the disk is interacting with
		return walls;
	}

	public static void test(String title, int numsteps, int sleepTime) {
		System.out.println(title);
		env.setTime(0);
		EnvironmentPanel ep = new EnvironmentPanel();
		ep.setEnvironment(env);
		//ep.getSettings().setCircleSymbol(2, 0.5);
		//ep.getSettings().setViewedRect(0.0,0.0,0.5,0.5);
		JPanel panel = new JPanel();
		frame.add(panel);
		frame.setTitle(title);
		frame.setVisible(true);
		frame.setSize(new Dimension(620, 700)); //620,700
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JLabel label = new JLabel(title);
		label.setFont(new Font(Font.SERIF, Font.BOLD, 36));
		panel.add(label);
		ep.setPreferredSize(new Dimension(600, 600)); //600,600
		panel.add(ep);
		frame.add(panel);
		frame.validate();
		final char[] key = new char[1];
		frame.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
				key[0] = e.getKeyChar();
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
			}

			@Override
			public void keyPressed(KeyEvent arg0) {
			}
		});
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		do {
			key[0] = 0;
			//		for (int i = 0; i < numsteps; i++) {
			long ts = System.currentTimeMillis();
			//env.doTimeStep(0.01);
			double dt = 0.01;
			env.doTimeStep(dt);
			env.setTime(env.getTime() + dt);
			synchronized (env) {
				for (DiskComplex merge[] : mergeList) {
					DiskComplex dc1 = merge[0].getRootInMergeTree();
					DiskComplex dc2 = merge[1].getRootInMergeTree();
					if (dc1 != dc2)
						dc1.merge(dc2);
				}
			}
			mergeList.clear();
			ep.repaint();
			long time = System.currentTimeMillis() - ts;
			long sleep = sleepTime - time;
			if (sleep > 0) {
				try {
					Thread.sleep(sleep);
				} catch (InterruptedException e) {
				}
			}
			//	}
			//	while (key[0] == 0) {
			//		try {
			//			Thread.sleep(100);
			//		} catch (InterruptedException e1) {
			//		}
			//	}
		} while (key[0] != ' ');
		panel.remove(ep);
		//env.clear();
		//		try {
		//		Thread.sleep(1000);
		//} catch (InterruptedException e) {
		//}
	}

	public static void setUniformFloor() {
		for (int i = 0; i < env.getFloor().getNumX(); i++)
			for (int j = 0; j < env.getFloor().getNumY(); j++)
				env.getFloor().setType(i, j, 0);
	}

	public static void setCheckerBoardFloor() {
		for (int i = 0; i < env.getFloor().getNumX(); i++)
			for (int j = 0; j < env.getFloor().getNumY(); j++)
				env.getFloor().setType(i, j, ((i + j) % 2));
	}

	public static void setQuadrantFloor() {
		for (int i = 0; i < env.getFloor().getNumX(); i++)
			for (int j = 0; j < env.getFloor().getNumY(); j++)
				env.getFloor().setType(i, j, (i >= 5) && (j <= 4) ? 3 : 0);
	}

	public static void sensor1() {
		DiskMaterial material1 = new DiskMaterial(200.0, 3);
		DiskMaterial material2 = new DiskMaterial(200.0, 4);

		DiskType diskType1 = new DiskType(material1);
		Set<DiskMaterial> materials = new HashSet<DiskMaterial>();
		materials.add(material1);
		Sensor sensor = new ColorSensitiveDirectionSensor(env, 0, 0.5 * Math.PI, 0.3, materials);
		//visionSensor sensor = new visionSensor(0, 0.5, 0, 0.5 * Math.PI, d ,env.getDiskGrid());
		DiskType diskType2 = new DiskType(material2, sensor);

		DiskComplexEnsemble dce = env.getDiskComplexesEnsemble();

		DiskComplex dc1 = dce.createNewDiskComplex();
		DiskComplex dc2 = dce.createNewDiskComplex();

		dce.addNewDisk(dc1, 0.2, 0.6, 0.02, 0.0, diskType1);
		dce.addNewDisk(dc2, 0.8, 0.5, 0.03, 0.0, diskType2);

		dc1.applyImpulse(0.02, 0.0, 0.2, 0.45);
		dc2.applyImpulse(-0.02, 0.0, 0.8, 0.49);
	}

	private static void setElasticity(double elasticity) {
		env.getPhysicsParameters().setDisk2DiskElasticty(elasticity);
		env.getPhysicsParameters().setDisk2WallElasticty(elasticity);
	}

	private static void setFriction(final double frictionConstant) {
		env.getPhysicsParameters().setFrictionModel(new GlobalFrictionModel(frictionConstant));
	}

}
