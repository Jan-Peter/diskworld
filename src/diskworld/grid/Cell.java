package diskworld.grid;

import java.util.HashSet; 
import java.util.Set;

import diskworld.diskcomplexes.Disk; 

/**
 * @author Jan
 * 
 */
public class Cell {

	private Set<Disk> intersectingDisks;
	private int[] index;

	/**
	 * Creates a CellDisks object
	 * 
	 * @param colIndex
	 *            x index of cell
	 * @param rowIndex
	 *            y index of cell
	 */
	public Cell(int colIndex, int rowIndex) {
		intersectingDisks = new HashSet<Disk>();
		index = new int[] { colIndex, rowIndex };
	}

	/**
	 * Access to the set of intersecting disks
	 * 
	 * @return set of disks
	 */
	public Set<Disk> getIntersectingDisks() {
		return intersectingDisks;
	}

	/**
	 * Access to the index
	 * 
	 * @return array of length 2 of form {colIndex, rowIndex}
	 */
	public int[] getIndex() {
		return index;
	}
	

}
