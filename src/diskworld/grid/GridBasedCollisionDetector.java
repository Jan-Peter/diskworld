package diskworld.grid;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import diskworld.diskcomplexes.Collision;
import diskworld.diskcomplexes.Disk;
import diskworld.diskcomplexes.DiskComplex;
import diskworld.environment.Wall;
import diskworld.interfaces.CollisionDetector;
import diskworld.shape.WallShape;

public class GridBasedCollisionDetector implements CollisionDetector {

	private GridWithDiskMap grid;
	private Map<Wall, List<Cell>> wallCells;

	public GridBasedCollisionDetector(GridWithDiskMap grid, LinkedList<Wall> walls) {
		this.grid = grid;
		this.wallCells = new HashMap<Wall, List<Cell>>();
		for (Wall w : walls) {
			wallCells.put(w, grid.getCellsIntersectingWithShape(new WallShape(w)));
		}
	}

	@Override
	public LinkedList<Collision> getNonSelfCollisions(DiskComplex dc) {
		LinkedList<Collision> res = new LinkedList<Collision>();
		for (Iterator<Disk> i = dc.getDisks().iterator(); i.hasNext();) {
			Disk d1 = i.next();
			List<Cell> cells = grid.getCellsIntersectingWithDisk(d1);
			for (Disk d2 : grid.getDisksIntersectingWithCells(cells)) {
				if (!d2.belongsTo(dc)) {
					Collision c = Collision.diskCollision(d1, d2);
					if (c != null) {
						res.add(c);
					}
				}
			}
		}
		for (Entry<Wall, List<Cell>> e : wallCells.entrySet()) {
			Wall wall = e.getKey();
			List<Cell> cells = e.getValue();
			for (Disk d : grid.getDisksIntersectingWithCells(cells)) {
				if (d.belongsTo(dc)) {
					Collision c = Collision.wallCollision(d, wall);
					if (c != null) {
						res.add(c);
					}
				}
			}
		}
		return res;
	}

	@Override
	public LinkedList<Collision> getNonSelfCollisions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean hasSelfCollisions(DiskComplex diskComplex) {
		// TODO Auto-generated method stub
		return false;
	}

}
