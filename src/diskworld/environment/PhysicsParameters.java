package diskworld.environment;

import diskworld.diskcomplexes.DiskMaterial;
import diskworld.interfaces.FrictionModel;

public class PhysicsParameters {
	
	private double disk2diskElasticity;
	private double disk2WallElasticity;
	private FrictionModel frictionModel;

	public PhysicsParameters(FrictionModel frictionModel) {
		disk2diskElasticity = 0.9;
		disk2WallElasticity = 0.9;
		this.frictionModel = frictionModel;
	}

	public PhysicsParameters() {
		this(new GlobalFrictionModel(0.0));
	}

	public double getDisk2DiskElasticty(DiskMaterial diskMaterial1, DiskMaterial diskMaterial2) {
		return disk2diskElasticity;
	}

	public double getDisk2WallElasticty(DiskMaterial diskMaterial) {
		return disk2WallElasticity;
	}
	
	public FrictionModel getFrictionModel() {
		return frictionModel;
	}
	
	public void setDisk2DiskElasticty(double disk2diskElasticity) {
		this.disk2diskElasticity = disk2diskElasticity;
	}

	public void setDisk2WallElasticty(double disk2WallElasticity) {
		this.disk2WallElasticity = disk2WallElasticity;
	}
	
	public void setFrictionModel(FrictionModel frictionModel) {
		this.frictionModel = frictionModel;
	}
	
}
