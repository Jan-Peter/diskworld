package diskworld.environment;

import diskworld.diskcomplexes.Disk;
import diskworld.interfaces.FrictionModel;
import diskworld.visualization.Log;

// the same as GlobalFrictionModel
// but getGlobalFrictionCoefficient should not be called
// and frictionIsGlobal is false

public abstract class LocalFrictionModel implements FrictionModel {
	
	
	// constructor missing ???
	
	
	
	@Override
	public double getGlobalFrictionCoefficient() {
		// should never be called for local friction constant
		Log.warning("getFrictionForce() should not be called for frictionIsGlobal()==false");
		return 0.0;
	}
	
	@Override
	public boolean frictionIsGlobal() {
		return false;
	}

	// computes the frictionConstant i guess
	abstract protected double getFloorContactCoefficient(Disk d); //????? why different in GlobalFrictionModel
	
	
	
	@Override
	public double getFloorContact(Disk d) {
		return d.getMass()*getFloorContactCoefficient(d);
	}
	
	
	
	@Override
	public double[] getFrictionForce(Disk d, double speedx, double speedy) {
		double f = -getFloorContact(d)/Math.sqrt(speedx*speedx+speedy*speedy); // friction constant * mass of disk d divided by the sqrt of speedx^2 and speedy^2
		double force[] = new double[2];
		force[0] = f*speedx;
		force[1] = f*speedy;
		return force;
    }

}
