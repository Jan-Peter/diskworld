package diskworld.environment;

import diskworld.diskcomplexes.Disk;
import diskworld.interfaces.FrictionModel;

// same as LocalFricitonModel
// but frictionIsGlobal is true
// and getFloorContact is computed differently (why?)

public class GlobalFrictionModel implements FrictionModel {

	private double frictionConstant;

	// constructor
	public GlobalFrictionModel(double frictionConstant) {
		this.frictionConstant = frictionConstant;
	}
	
	
	// returns the frictionConstant
	@Override
	public double getGlobalFrictionCoefficient() {
		return frictionConstant;
	}
	
	@Override
	public boolean frictionIsGlobal() {
		return true;
	}

	@Override
	public double getFloorContact(Disk d) {
		return d.getMass()*frictionConstant;
	}

	@Override
	public double[] getFrictionForce(Disk d, double speedx, double speedy) {
		double f = -getFloorContact(d)/Math.sqrt(speedx*speedx+speedy*speedy); // friction constant * mass of disk d divided by the length of 
																			   // a speed vector represented by speedx and speedy	
		double force[] = new double[2];
		force[0] = f*speedx;
		force[1] = f*speedy;
		return force;
	}

}
