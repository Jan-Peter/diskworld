package diskworld.environment;

import diskworld.interfaces.CollidableObject;
import diskworld.linalg2D.Line;

// a wall is represented as a line object witch a certain thickness
// it has parameters: thickness (d^2), thicknessX (dx^2) and thicknessY (dy^2)

public class Wall implements CollidableObject {
	private Line line;
	private double d,dx,dy;

	public Wall(Line line, double thickness) {
		this.line = line;
		calculateOrthogonal(thickness);
	}
	

	// computes d,dy and dx
	private void calculateOrthogonal(double thickness) {
		d = thickness*0.5;
		double f = d/line.getLength();
		dy = line.getDeltaX()*f;
		dx = -line.getDeltaY()*f;
	}

	public double getX1() {
		return line.getX1();
	}
	
	public double getY1() {
		return line.getY1();
	}

	public double getX2() {
		return line.getX2();
	}
	
	public double getY2() {
		return line.getY2();
	}

	public double getHalfThickness() {
		return d;
	}

	public double getHalfThicknessX() {
		return dx;
	}

	public double getHalfThicknessY() {
		return dy;
	}

}
