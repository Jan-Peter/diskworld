package diskworld.environment;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import diskworld.diskcomplexes.Disk;
import diskworld.diskcomplexes.DiskComplex;
import diskworld.diskcomplexes.DiskComplexEnsemble;
import diskworld.diskcomplexes.FullSearchCollisionDetector;
import diskworld.grid.Grid;
import diskworld.grid.GridBasedCollisionDetector;
import diskworld.grid.GridWithDiskMap;
import diskworld.interfaces.Actuator;
import diskworld.interfaces.CollisionDetector;
import diskworld.interfaces.Sensor;
import diskworld.visualization.PaintableEnvironmentClone;

public class Environment {

	private static final boolean USE_GRID_BASED_COLLISION_DETECTION = false;
	private static final double SIZE_OF_GRID_CELL_X = 10;
	private static final double SIZE_OF_GRID_CELL_Y = 10;

	private final Floor floor;
	private final DiskComplexEnsemble diskComplexes;
	private final GridWithDiskMap diskGrid;
	private final Grid floorGrid;
	private final LinkedList<Wall> walls;
	private double time;
	private PhysicsParameters physicsParameters;

	public Environment(int floorSizex, int floorSizey, double floorScale, Collection<Wall> wallCollection, PhysicsParameters physicsParameters) {
		floor = new Floor(floorSizex, floorSizey, floorScale);
		walls = new LinkedList<Wall>(wallCollection);
		diskGrid = new GridWithDiskMap(floor.getMaxX(), floor.getMaxY(), SIZE_OF_GRID_CELL_X, SIZE_OF_GRID_CELL_Y);
		floorGrid = new Grid(floor.getMaxX(), floor.getMaxY(), floor.getNumX(), floor.getNumY());
		CollisionDetector collisionDetector;
		if (USE_GRID_BASED_COLLISION_DETECTION) {
			collisionDetector = new GridBasedCollisionDetector(diskGrid, walls);
		} else {
			collisionDetector = new FullSearchCollisionDetector(walls);
		}
		diskComplexes = new DiskComplexEnsemble(collisionDetector, floor);
		diskComplexes.addChangeListener(diskGrid);
		if (!USE_GRID_BASED_COLLISION_DETECTION) {
			diskComplexes.addChangeListener((FullSearchCollisionDetector) collisionDetector);
		}
		this.physicsParameters = physicsParameters;
		time = 0.0;
	}

	public Environment(int floorSizex, int floorSizey, double floorScale, Collection<Wall> wallCollection) {
		this(floorSizex, floorSizey, floorScale, wallCollection, new PhysicsParameters());
	}

	public Environment(int floorSizex, int floorSizey, double floorScale) {
		this(floorSizex, floorSizey, floorScale, null, new PhysicsParameters());
	}

	public Floor getFloor() {
		return floor;
	}

	public DiskComplexEnsemble getDiskComplexesEnsemble() {
		return diskComplexes;
	}

	public synchronized PaintableEnvironmentClone getPaintableClone() {
		return new PaintableEnvironmentClone(this);
	}

	public synchronized void doTimeStep(double dt) {
		performActuatorEffects();
		while (dt > 0.0) {
			double timestep = Math.min(dt, diskComplexes.getMaxTimeStep());
			diskComplexes.doTimeStep(timestep, physicsParameters);
			time += timestep;
			dt -= timestep;
		}
		performSensorMeasurements();
	}

	public Collection<DiskComplex> getDiskComplexes() {
		return diskComplexes.getDiskComplexes();
	}

	public PhysicsParameters getPhysicsParameters() {
		return physicsParameters;
	}

	public List<Wall> getWalls() {
		return walls;
	}

	/*
	 * public void clear() { diskComplexes.clear(); floor.clear(); }
	 */

	public double getTime() {
		return time;
	}

	public void setTime(double time) {
		this.time = time;
	}

	public double getMaxX() {
		return floor.getMaxX();
	}

	public double getMaxY() {
		return floor.getMaxY();
	}

	public Grid getFloorGrid() {
		return floorGrid;
	}

	public GridWithDiskMap getDiskGrid() {
		return diskGrid;
	}

	private void performActuatorEffects() {
		for (Disk d : diskComplexes.getActuatorDisks()) {
			Actuator actuator = d.getDiskType().getActuator();
			actuator.evaluateEffect(d, floor, d.getActivity());
		}
	}

	private void performSensorMeasurements() {
		for (Disk d : diskComplexes.getSensorDisks()) {
			Sensor[] sensors = d.getDiskType().getSensors();
			for (int i = 0; i < sensors.length; i++) {
				sensors[i].doMeasurement(d, d.getSensorMeasurements()[i]);
			}
		}
	}

}
