package diskworld.environment;

public class Floor {
	private final int numx, numy;
	private final byte[][] type;
	private final double scale;

	public Floor(int numColumns, int numRows, double scale) {
		numx = numColumns;
		numy = numRows;
		type = new byte[numx][numy];
		this.scale = scale;
	}

	public int[] getSize() {
		return new int[] { numx, numy };
	}

	public int getNumX() {
		return numx;
	}

	public int getNumY() {
		return numy;
	}

	public double getMaxX() {
		return numx * scale;
	}

	public double getMaxY() {
		return numy * scale;
	}

	public double getPosX(int x) {
		return x * scale;
	}

	public double getPosY(int y) {
		return y * scale;
	}

	public int getType(int x, int y) {
		return (x < 0) || (x >= numx) || (y < 0) || (y >= numy) ? 0 : type[x][y] & 0xff;
	}

	public int getType(double x, double y) {
		return getType((int) Math.floor(x / scale), (int) Math.floor(y / scale));
	}

	public void setType(int x, int y, int type) {
		this.type[x][y] = (byte) (type & 0xff);
	}

	public void fill(int type) {
		for (int i = 0; i < numx; i++)
			for (int j = 0; j < numy; j++)
				this.type[i][j] = (byte) (type & 0xff);
	}

	public void clear() {
		fill((byte) 0);
	}

	public Floor createClone() {
		Floor clone = new Floor(numx, numy, scale);
		for (int i = 0; i < numx; i++)
			clone.type[i] = type[i].clone();
		return clone;
	}

}
