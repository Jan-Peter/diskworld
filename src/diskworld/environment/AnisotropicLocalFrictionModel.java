package diskworld.environment;

import diskworld.diskcomplexes.Disk;


// richtungsabh�ngige (anisotrope) lokale Reibung

public abstract class AnisotropicLocalFrictionModel extends LocalFrictionModel {
		
	abstract protected double getLongitudalFrictionCoefficient(Disk d);

	abstract protected double getTransversalFrictionCoefficient(Disk d);
	
	/**
	 * Calculates 2-dimensional friction force
	 * @param d the disk to calculate the friction for
	 * @param speedx speed in x-direction
	 * @param speedy speed in y-direction
	 * @return 2-D friction-force vector
	 */
	@Override
	public double[] getFrictionForce(Disk d, double speedx, double speedy) {
		double a = d.getAngle(); // steht in Disk, wei� nich welcher Winkel
		double s = Math.sin(a);
		double c = Math.cos(a);
		double longitudal = speedx*c+speedy*s;
		double transversal = -speedx*s+speedy*c;
		longitudal *= -getLongitudalFrictionCoefficient(d);
		transversal *= -getTransversalFrictionCoefficient(d);
		double force[] = new double[2];
		force[0] = longitudal*c-transversal*s;    
		force[1] = longitudal*s+transversal*c; 
		return force;
    }

}
