# README #

### Diskworld ###

Diskworld is a 2D Simulation-Environment based on disks.

We are currently developing some extra features, the basic functionality however should work.


### Who's behind it ###

Currently we are three undergraduate students writing their Bachelor-thesis in Cognitive Science based on Diskworld. The responsible supervisor is Jan Kneissler [1]. It's also mainly his code we're extending here.


[1] http://www.wsi.uni-tuebingen.de/lehrstuehle/cognitive-modeling/staff/staff/jan-kneissler.html